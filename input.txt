int factorial(int x) {
    int fact = 1;

    for(int i = 1; i <= x; i = i + 1) {
        fact = fact * i;
    }

    return fact;
}

void main() {
    int result = factorial(5);
    return;
}