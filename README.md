# KIV/FJP - Semestrální práce
## Struktura projektu
- src - zdrojové kódy
- pl0lib - zdrojové kódy pomocných knihovních metod
- nsd.txt - pomocný zdrojový kód pro práci s racionálními čísly 
- examples - příklady použití
- pom.xml - překlad projektu
- run.bat - skript pro build programu pro Windows
- run.sh - skript pro build programu pro Linux

## Spuštění
Pro překlad projektu je nutné mít nainstalovanou Javu a Maven. Použitím příkazu 

>mvn clean install

se vytvoří složka target obsahující JAR soubory. Pro spuštění programu slouží příkaz:

>java -jar target/JavaToPL0-1.0-jar-with-dependencies.jar <vstupní soubor> <výstupní soubor>

## Ukázkový příklad
### Linux

Prvně je nutné zajistit, že soubor **run.sh** je spustitelný soubor. Pokud skript nelze spustit, je nutné nastavit práva:

>chmod u+x run.sh

Následně se skript spustí pomocí příkazu:

>./run.sh

Vytvoří se textový soubor **output.txt**, který obsahuje instrukce PL0 z příkladu *examples/methodCall.txt*.

### Windows

Po spuštěním souboru **run.bat** se vytvoří textový soubor **output.txt**, který obsahuje instrukce PL0 z příkladu *examples/methodCall.txt*.


