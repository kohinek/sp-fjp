package fjp.sp.simplegrammar.utils;

public class Wrapper<T> {

    //slouzi k predavani navratove hodnoty

   private T ref;


    public Wrapper(T ref) {
        this.ref = ref;
    }

    public T getRef() {
        return ref;
    }

    public void setRef(T ref) {
        this.ref = ref;
    }
}
