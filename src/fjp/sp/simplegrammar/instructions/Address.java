package fjp.sp.simplegrammar.instructions;


public interface Address {
    //interface pro adresu
    //muze byt seznam labelu, prazdny, nebo konkretni hodnota v intu (instruction integer wrapper)

    //reakce na prelozeni labelu
    //prazdny label (null address) vrati hodnotu radky
    //uz nastavena adresa vrati sama sebe
    //label list nastavi hodnotu jako cislo radky a pregenerujeme vsechny labely, ktere na nej odkazuji
    InstructionIntegerWrapper translateLabelToLineNumber(int lineNumber, InstructionSet instructionSet);
}
