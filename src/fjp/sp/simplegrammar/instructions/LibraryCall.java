package fjp.sp.simplegrammar.instructions;

import fjp.sp.simplegrammar.library.LibraryLoader;

import java.util.List;

public class LibraryCall implements Instruction {

    //volani knihovni metody, muze byt instrukce
    private String libraryName;

    public LibraryCall(String libraryName) {
        this.libraryName = libraryName;
    }

    //zpracovani prevedu na library loader, ktery drzi informace o vsech knihovnich metodach
    @Override
    public void processLibraryCall(List<Instruction> instructionsForLibrary, List<InstructionWOLibcall> instructionForCall) {
        LibraryLoader loader = LibraryLoader.getInstance();
        loader.loadLibrary(libraryName, instructionsForLibrary, instructionForCall);
    }
}
