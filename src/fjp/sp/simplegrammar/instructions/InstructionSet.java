package fjp.sp.simplegrammar.instructions;

import java.util.ArrayList;
import java.util.List;

public class InstructionSet {

    //seznam zdrojovych instrukci
    private List<Instruction> source;

    //seznam instrukci bez libcallu (tj na konci mame vygenerovane knihovni metody a libcall jsou nahrazene CAL)
    private List<InstructionWOLibcall> temp;

    //vystup
    private List<PL0Instruction> translation;

    //konstruktor
    public InstructionSet(List<Instruction> source) {
        this.source = source;
    }

    //zpracovani labelu, pokud je mame jako hodnotu instrukce
    //kdyz zpracuji adresu labelu, proiteruju instrukce a vsechny, ktere odkazuji na dany label zmenim na adresu
    void changeLabelJumpToLineNumber(Label label, int lineNumber) {
        for(PL0Instruction inst : translation) {
            inst.setValue(inst.getValue().changeLabelToLineNumber(label, lineNumber));
        }
    }


    //prelozi instrukce ze source na translation
    public void translate() {
        //sem budu pridavat instrukce
        translation = new ArrayList<>();

        //zpracuji libcally
        processLibCalls();

        //pokud mam vice labelu za sebou, smrsknu do jednoho label listu
        removeLabelInstructions();

        //pro kazdou instrukci pridavam jeji adresu, pokud narazim na label, zmenim vsechny odkazy na dany label na cislo
        int lineCount = 0;

        for(PL0Instruction instruction : translation) {
            instruction.setAddress(instruction.getAddress().translateLabelToLineNumber(lineCount, this));
            lineCount++;
        }
    }

    //pokud mam vice labelu za sebou, smrsknu do jednoho label listu
    private void removeLabelInstructions() {
        List<Label> tempLabels = new ArrayList<>();

        for(InstructionWOLibcall inst : temp) {
            tempLabels = inst.processLabelInstruction(tempLabels, translation);
        }
    }

    //zpracovani volani knihovnich metod
    private void processLibCalls() {
        temp = new ArrayList<>();
        List<Instruction> libraryInstructions = new ArrayList<>();

        //vystupem ma byt seznam list, ktery obsahuje instrukce bez lib callu
        //libcally potencialne obsahuji dalsi libcally, takze je nemuzeme rovnou pridat, budeme si je tedy odkladat na stranu

        //v 1. kole prelozime libcally v puvodnim seznamu, do library instructions budu generovat knihovni instrukce
        for(Instruction instruction : this.source) {
            instruction.processLibraryCall(libraryInstructions, temp);
        }

        //nasledne budeme dokola zpracovavat knihovni instrukce, dokud nebudou obsahovat zadne dalsi libcally
        appendLibCalls(libraryInstructions);

        //v poslednim kole pridame knihovni instrukce do vystupu
        for(Instruction instruction : libraryInstructions) {
            //knihovny jsou pridane, na null nikdo nesahne
            instruction.processLibraryCall(null, temp);
        }
    }

    private void appendLibCalls(List<Instruction> libraryInstructions) {

        List<InstructionWOLibcall> toRemove = new ArrayList<>();
        //zpracujeme instrukce, nove knihovni instrukce pridavame na konec seznamu, volani budeme ignorovat
        for(int i = 0; i < libraryInstructions.size(); i++) {
            libraryInstructions.get(i).processLibraryCall(libraryInstructions, toRemove);
        }
    }


    //seznam s instrukcemi na string
    private String listToString(List<? extends Instruction> list) {
        StringBuilder builder = new StringBuilder();

        for(Instruction inst : list) {
            builder.append(inst);
            builder.append("\n");
        }

        return builder.toString();
    }

    public String translationToString() {
        return listToString(translation);
    }

    public String sourceToString() {
        return listToString(source);
    }


}
