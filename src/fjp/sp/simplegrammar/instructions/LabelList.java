package fjp.sp.simplegrammar.instructions;

import java.util.List;

public class LabelList implements Address {

    //seznam labelu jako adresa

    private List<Label> labels;

    LabelList(List<Label> labels) {
        this.labels = labels;
    }


    //prelozeni labelu na cisla
    @Override
    public InstructionIntegerWrapper translateLabelToLineNumber(int lineNumber, InstructionSet instructionSet) {
        for(Label label : labels) {
            //proiteruju instruction set a hledam v nem odkazy na moje labely
            instructionSet.changeLabelJumpToLineNumber(label, lineNumber);
        }

        //vratim hodnotu radku
        return new InstructionIntegerWrapper(lineNumber);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        boolean makeComma = false;

        for(Label label : labels) {
            if(makeComma) {
                builder.append(", ");
            }

            builder.append(label);
            makeComma = true;
        }

        return builder.toString();
    }
}
