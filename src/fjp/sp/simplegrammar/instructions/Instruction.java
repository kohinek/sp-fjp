package fjp.sp.simplegrammar.instructions;

import java.util.List;

public interface Instruction {

    //instrukce
    //muze byt pl0 instrukce, library call nebo label (library call a label dedi z instruction wo libcall,
    // coz je interface ktery implementuje tento interface


    //reakce na zpracovani library callu
    //do library instruction se pridaji instrukce generovane knihovnou
    //instruction set je pak da na konec outputu
    //do actual instruction se prevedou samy instrukce

    //pl0 instrukce a label do library instructionu negeneruje nic a do actual instructionu akorat pridaji samy sebe
    //library call do library instruction vygeneruje knihovni metodu (pokud uz neni vygenerovana)
    //a do actual instructionu prida call na label, odkazujici na knihovni metodu
    //a int nastavujici pocet parametru (viz trida library loader)
    void processLibraryCall(List<Instruction> libraryInstruction, List<InstructionWOLibcall> actualInstructions);
}
