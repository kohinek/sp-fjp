package fjp.sp.simplegrammar.instructions;

import java.util.List;

public class Label implements InstructionValue, InstructionWOLibcall {

    //label, muze byt instrukce nebo hodnota instrukce

    //pocitani id
    private static int labelCount = 0;
    private int labelId;


    //konstruktor
    //id nastavim na id posledniho + 1
    public Label() {
        labelId = labelCount;
        labelCount++;
    }




    @Override
    public String toString() {
        return "L" + labelId;
    }


    //pokud je label jako nase specialni instrukce
    //pridam label do seznamu aktualnich labelu, do prekladu nebudu pridavat zadnou instrukci
    @Override
    public List<Label> processLabelInstruction(List<Label> currentLabels, List<PL0Instruction> translationInstructions) {
        currentLabels.add(this);
        return currentLabels;
    }


    //pokud je label jako hodnota jine instrukce (skoku)
    //uz znam hodnotu skoku, muzu zmenit na tu konkretni hodnotu
    @Override
    public InstructionValue changeLabelToLineNumber(Label label, int lineNumber) {
        if(label.labelId == this.labelId) {
            return new InstructionIntegerWrapper(lineNumber);
        } else {
            return this;
        }

    }

    //viz inteface instruction
    @Override
    public void processLibraryCall(List<Instruction> instructions, List<InstructionWOLibcall> actualInstructions) {
        actualInstructions.add(this);
    }
}
