package fjp.sp.simplegrammar.instructions;

import java.util.List;

public interface InstructionWOLibcall extends Instruction {

    //instrukce, mezi ktere nepocitame libcall

    //tzn. label a pl0 instrukce

    //zpracuji label
    //labely na cisla prekladam az jako adresu instrukce
    //v tomto bode pouze labely, ktere jdou za sebou smrsknu do jednoho seznamu labelu
    //a pridam jako adresu nasledujici instrukce
    List<Label> processLabelInstruction(List<Label> currentLabels, List<PL0Instruction> translationInstructions);
}
