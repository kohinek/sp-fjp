package fjp.sp.simplegrammar.instructions;

public interface InstructionValue {

    //hodnota instrukce, muze byt bud cislo nebo label

    //pokud znam hodnotu labelu, na ktery ukazuje hodnota instrukce, zmenim jej na cislo
    //cislo vrati samo sebe
    //pokud ukazuji na label, ktery nezpracovavam, tak vratim sam sebe
    InstructionValue changeLabelToLineNumber(Label label, int lineNumber);
}
