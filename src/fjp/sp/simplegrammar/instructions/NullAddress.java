package fjp.sp.simplegrammar.instructions;

public class NullAddress implements  Address {

    //prazdna, dosud nenastavena adresa instrukce


    @Override
    public String toString() {
        return "NS";
    }


    //v tomto bode uz adresu znam, vratim hodnotu
    @Override
    public InstructionIntegerWrapper translateLabelToLineNumber(int lineNumber, InstructionSet instructionSet) {
        return new InstructionIntegerWrapper(lineNumber);
    }
}
