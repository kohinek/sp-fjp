package fjp.sp.simplegrammar.instructions;

import java.util.ArrayList;
import java.util.List;

public class PL0Instruction implements InstructionWOLibcall {

    //PL0 instrukce

    //obsahuje:
    //adresu
    private Address address;
    //typ instrukce
    private InstructionType instructionType;
    //level
    private int level;
    //hodnotu
    private InstructionValue value;

    //konstuktory pro vsechny mozne hodnoty volani
    public PL0Instruction(InstructionType instructionType, int level, InstructionValue value) {
        this.address = new NullAddress();
        this.instructionType = instructionType;
        this.level = level;
        this.value = value;
    }

    public PL0Instruction(InstructionType instructionType, int level, int value) {
        this(instructionType, level, new InstructionIntegerWrapper(value));
    }

    public PL0Instruction(InstructionType instructionType, InstructionValue value) {
        this(instructionType, 0, value);
    }

    public PL0Instruction(InstructionType instructionType, int value) {
        this(instructionType, 0, new InstructionIntegerWrapper(value));
    }

    //vytvoreni instrukce z jine instrukce
    public PL0Instruction(PL0Instruction another) {
        this.address = another.address;
        this.instructionType = another.instructionType;
        this.level = another.level;
        this.value = another.value;
    }

    Address getAddress() {
        return address;
    }



    InstructionValue getValue() {
        return value;
    }

    void setValue(InstructionValue value) {
        this.value = value;
    }

    void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return address + " " + instructionType + " " + level + " " + value;
    }


    //viz interface instructio wo libcall
    @Override
    public List<Label> processLabelInstruction(List<Label> currentLabels, List<PL0Instruction> translationInstructions) {
        PL0Instruction newInst = new PL0Instruction(this);
        translationInstructions.add(newInst);

        if(currentLabels.size() > 0) {
            newInst.setAddress(new LabelList(currentLabels));
            return new ArrayList<>();
        } else {
            return currentLabels;
        }
    }

    //viz interface instruction
    @Override
    public void processLibraryCall(List<Instruction> instructions, List<InstructionWOLibcall> actualInstructions) {
        actualInstructions.add(this);
    }
}
