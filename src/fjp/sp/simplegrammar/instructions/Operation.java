package fjp.sp.simplegrammar.instructions;

public enum Operation implements InstructionValue {

    //operace, tj hodnota instrukce OPR

    UMINUS, PLUS, MINUS, MUL, DIV, MOD, ODD, EQ, NOT_EQ, L, GE, G, LE;


    @Override
    public String toString() {
        return "" + getValue();
    }

    public int getValue() {
        switch(this) {
            case UMINUS:
                return 1;
            case PLUS:
                return 2;
            case MINUS:
                return 3;
            case MUL:
                return 4;
            case DIV:
                return 5;
            case MOD:
                return 6;
            case ODD:
                return 7;
            case EQ:
                return 8;
            case NOT_EQ:
                return 9;
            case L:
                return 10;
            case GE:
                return 11;
            case G:
                return 12;
            case LE:
                return 13;
        }

        return 0;
    }

    //operace muze byt hodnota instrukce
    @Override
    public InstructionValue changeLabelToLineNumber(Label label, int lineNumber) {
        return this;
    }
}
