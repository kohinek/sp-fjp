package fjp.sp.simplegrammar.instructions;

public class InstructionIntegerWrapper implements InstructionValue, Address {

    //predstavuje cisla v instrukcich
    //muze byt jako adresa
    //nebo jako hodnota instrukce
    private int value;

    public InstructionIntegerWrapper(int value) {
        this.value = value;
    }


    @Override
    public String toString() {
        return "" + value;
    }

    //viz interface address
    @Override
    public InstructionIntegerWrapper translateLabelToLineNumber(int lineNumber, InstructionSet instructionSet) {
        return this;
    }

    //viz interface instruction value
    @Override
    public InstructionValue changeLabelToLineNumber(Label label, int lineNumber) {
        return this;
    }
}
