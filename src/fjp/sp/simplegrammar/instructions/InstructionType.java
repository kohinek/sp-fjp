package fjp.sp.simplegrammar.instructions;


//typ instrukce
public enum InstructionType {
    LIT, OPR, LOD, STO, CAL, INT, JMP, JMC, RET;

    @Override
    public String toString() {
        return this.name();
    }


}
