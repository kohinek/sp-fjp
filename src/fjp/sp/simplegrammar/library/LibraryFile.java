package fjp.sp.simplegrammar.library;

import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.instructions.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class LibraryFile {

    //konstanty, cesta ke knihovnim funkcim
    private static final String LIBRARY_PATH = "pl0lib";
    //pripona souboru s knihovni funkci
    private static final String LIBRARY_FILE_EXTENSION = ".txt";

    //buffer pro cteni
    private BufferedReader reader;

    //jmeno knihovny
    private String name;

    //label pro skok na knihovni funkci
    private Label methodLabel;

    //o kolik se zvetsi navratova hodnota oproti vstupnim parametrum
    //napr fce ratioeq prijima 2 cisla typu ratio, coz je velikost 4
    //a vraci 1 hodnotu typu boolean, coz je velikost 1
    //change tak bude -3
    //je definovany jako prvni radka knihovny
    private int change;

    //tabulka definovanych labelu v knihovni funkci
    private Map<String, Label> labels = new HashMap<>();

    int getChange() {
        return change;
    }


    //konstruktor
    //nacteme reader, vytvorime label, nastavime jmeno
    LibraryFile(String name) {
        this.name = name;
        String path = LIBRARY_PATH + "/" + name + LIBRARY_FILE_EXTENSION;
        try {
            FileReader file = new FileReader(path);
            this.reader = new BufferedReader(file);
        } catch (FileNotFoundException e) {
            ErrorHandler.getInstance().error("Compiler error: can't find " + path);
        }

        this.methodLabel = new Label();

    }

    Label getMethodLabel() {
        return methodLabel;
    }


    //precte instrukce v souboru do seznamu
    void readInstructions(List<Instruction> instructions) {
        //na zacatek dame label
        instructions.add(this.methodLabel);
        try {
            //nacteme change
            String line = this.reader.readLine();
            readFirstLine(line);

            //pote nacitame jednotlive radky
            line = this.reader.readLine();
            while(line != null) {
                readInstruction(line, instructions);
                line = this.reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //nacteni change
    private void readFirstLine(String line) {
        this.change = Integer.parseInt(line);
    }

    //nacteni instrukce
    private void readInstruction(String instruction, List<Instruction> instructions) {
        String[] words = instruction.split(" ");
        if(words.length == 0) {
            return;
        }

        //rozeznavame libcall, label nebo klasickou instrukci pl0
        switch(words[0].toLowerCase()) {
            case "libcall":
                if(words.length < 2) {
                    ErrorHandler.getInstance().error("Error processing " + this.name + " library. Not enough libcall arguments.");
                }
                instructions.add(new LibraryCall(words[1]));
                break;
            case "label":
                if(words.length < 2) {
                    ErrorHandler.getInstance().error("Error processing " + this.name + " library. Not enough label arguments.");
                }
                instructions.add(getLocalLabel(words[1]));
                break;
            default:
                InstructionType type = InstructionType.valueOf(words[0]);
                if(words.length < 3) {
                    ErrorHandler.getInstance().error("Error processing " + this.name + " library. Not enough instruction arguments.");
                }

                try {
                    //pokud mame 3. hodnotu jako integer
                    instructions.add(new PL0Instruction(type, Integer.parseInt(words[1]), Integer.parseInt(words[2])));
                } catch (NumberFormatException e) {
                    //pokud mame 3. hodnotu jako string, prevedeme na label
                    instructions.add(new PL0Instruction(type, Integer.parseInt(words[1]), getLocalLabel(words[2])));
                }

                break;
        }
    }

    //nacteni labelu definovaneho v knihovni funkci
    private Label getLocalLabel(String name) {
        if(!this.labels.containsKey(name)) {
            Label label = new Label();
            this.labels.put(name, label);
            return label;
        }

        return this.labels.get(name);
    }


}
