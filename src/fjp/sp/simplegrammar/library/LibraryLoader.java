package fjp.sp.simplegrammar.library;


import fjp.sp.simplegrammar.instructions.Instruction;
import fjp.sp.simplegrammar.instructions.InstructionWOLibcall;
import fjp.sp.simplegrammar.instructions.InstructionType;
import fjp.sp.simplegrammar.instructions.PL0Instruction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LibraryLoader {

    //trida ktera nacita knihovni funkce

    //tabulka funkci
    private Map<String, LibraryFile> map = new HashMap<>();


    //singleton
    private static LibraryLoader instance = new LibraryLoader();
    public static LibraryLoader getInstance() {
        return instance;
    }
    private LibraryLoader() {

    }


    //vygeneruje instrukci skoku a pripadne prida nakonec instrukce knihovni metody
    public void loadLibrary(String name, List<Instruction> instructionsForLibrary, List<InstructionWOLibcall> instructionForCall) {
        LibraryFile file;
        //pokud uz mame v tabulce knihovni metodu, vygenerujeme pouze skok
        //jinak nagenerujeme celou knihovni metodu ze souboru
        //obe varianty predavame do jinych seznamu
        if(map.containsKey(name)) {
            file = map.get(name);

        } else {
            file = new LibraryFile(name);
            file.readInstructions(instructionsForLibrary);
            map.put(name, file);
        }

        int change = file.getChange();

        //change ~ n
        //PRO n > 0 -> INT n; CAL (vice vystupnich parametru nez vstupnich) = musime vytvorit prostor pro vystupno
        //PRO n = 0 -> CAL (stejne vstupnich jako vystupnich)
        //PRO n < 0 -> CAL; INT n (vice vstupnich nez vystupnich) = na konci zahodime prebytecne vstupni parametry

        if(change > 0) {
            instructionForCall.add(new PL0Instruction(InstructionType.INT, file.getChange()));
        }
        instructionForCall.add(new PL0Instruction(InstructionType.CAL,1, file.getMethodLabel()));
        if(change < 0) {
            instructionForCall.add(new PL0Instruction(InstructionType.INT, file.getChange()));
        }

    }


}
