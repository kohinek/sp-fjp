package fjp.sp.simplegrammar.symbols;

import fjp.sp.simplegrammar.Type;

public class MethodReturnInfo {

    //info pro return statement
    //return statement diky tomu vi, jaky typ promenny muze return vratit a kam ji ma pripadne ulozit

    public MethodReturnInfo(int paramSize, Type methodType) {
        this.paramSize = paramSize;
        this.methodType = methodType;
    }

    public final int paramSize;
    public final Type methodType;
}
