package fjp.sp.simplegrammar.symbols;

import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.utils.Wrapper;

public class GlobalContext extends Context {


    //globalni context tabulky symbolu
    private static GlobalContext instance = new GlobalContext();



    public static GlobalContext getInstance() {
        return GlobalContext.instance;
    }

    private GlobalContext() {

    }

    //ziskani symbolu
    @Override
    protected Symbol getSymbol(String name, Wrapper<Integer> level) {
        //globalni context uz nema ve spaghetti stacku zadny context nad sebou, bud symbol obsahuje, nebo ne
        if(symbols.containsKey(name)) {
            level.setRef(0);
            return symbols.get(name);
        } else {
            ErrorHandler.getInstance().error("Can't find symbol " + name);
            return null;
        }
    }
}
