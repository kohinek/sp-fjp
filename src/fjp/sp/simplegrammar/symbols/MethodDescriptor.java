package fjp.sp.simplegrammar.symbols;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.instructions.Label;

public class MethodDescriptor implements Symbol {

    //descriptor metody

    //label vyuzivany ke skoku na zacatek metody
    private Label label;

    //navratova hodnota
    private Type type;

    //metody vysvetleny v definici rozhrani a ve tride context

    public MethodDescriptor(Type type) {
        this.type = type;
        this.label = new Label();
    }

    public Label getLabel() {
        return label;
    }

    @Override
    public Type getType() {
        return this.type;
    }

    @Override
    public MethodDescriptor call() {
        return this;
    }

    @Override
    public VarDescriptor varAssign() {
        ErrorHandler.getInstance().error("Can't assign to method");
        return null;
    }

    @Override
    public VarDescriptor varRetrieve() {
        ErrorHandler.getInstance().error("Can't retrieve value from method");
        return null;
    }

    @Override
    public int setAddress(int currentAddress) {
        return currentAddress;
    }

    @Override
    public int getAddress() {
        return 0;
    }

    @Override
    public int getLengthForParams() {
        return 0;
    }
}
