package fjp.sp.simplegrammar.symbols;

import fjp.sp.simplegrammar.utils.Wrapper;

public class LocalContext extends Context {

    //lokalni context spaghetti stacku
    //ma context nad sebou (v nasem pripade pouze globalni context)

    //nadrazeny context
    private Context parent;


    public LocalContext(Context parent) {
        this.parent = parent;
    }

    //ziskani symbolu
    protected Symbol getSymbol(String name, Wrapper<Integer> level) {
        //bud ho obsahuje sam lokalni context
        if(symbols.containsKey(name)) {
            level.setRef(0);
            return symbols.get(name);
        }

        //nebo se podivam do nadrazeneho contextu
        Symbol rvalue = parent.getSymbol(name, level);
        level.setRef(level.getRef() + 1);
        return rvalue;
    }


}
