package fjp.sp.simplegrammar.symbols;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.error.ErrorHandler;

public class VarDescriptor implements Symbol {

    //vysvetleno v rozhrani

    protected Type type;
    private int address;

    public VarDescriptor(Type type) {
        this.type = type;
    }

    @Override
    public Type getType() {
        return this.type;
    }

    @Override
    public MethodDescriptor call() {
        ErrorHandler.getInstance().error("Can't call a variable");
        return null;
    }

    @Override
    public VarDescriptor varAssign() {
        return this;
    }

    @Override
    public VarDescriptor varRetrieve() {
        return this;
    }

    @Override
    public int setAddress(int currentAddress) {
        this.address = currentAddress;
        return this.address + this.type.getLength();
    }

    @Override
    public int getAddress() {
        return this.address;
    }

    @Override
    public int getLengthForParams() {
        return this.type.getLength();
    }
}
