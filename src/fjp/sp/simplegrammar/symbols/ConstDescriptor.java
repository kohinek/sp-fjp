package fjp.sp.simplegrammar.symbols;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.error.ErrorHandler;

public class ConstDescriptor extends VarDescriptor {

    //polozka v tabulce symbolu pro konstantu
    private boolean assigned;

    public ConstDescriptor(Type type) {
        super(type);
        this.assigned = false;
    }

    //nastaveni promenne je dovoleno pouze 1, pote prekladac zahlasi chybu

    @Override
    public VarDescriptor varAssign() {
        if(!assigned) {
            assigned = true;
            return this;
        } else{
            ErrorHandler.getInstance().error("Can't assign a constant.");
            return null;
        }
    }

}
