package fjp.sp.simplegrammar.symbols;

import fjp.sp.simplegrammar.Type;

public interface Symbol {

    //symbol v tabulce symbolu

    //typ promenne nebo navratovy typ metody
    Type getType();

    //reakce na call, metoda vrati sebe, promenna zahlasi chybu
    MethodDescriptor call();

    //reakce na assignment, promenna vrati sama sebe, konstanta lze jen 1x, metoda zahlasi chybu
    VarDescriptor varAssign();

    //reakce na retrieval, promenna vrati sama sebe, metoda zahlasi chybu
    VarDescriptor varRetrieve();

    //nastaveni adresy v contextu, vyuzivane pri pridani symbolu do tabulky
    int setAddress(int currentAddress);

    //ziskani adresy
    int getAddress();

    //vyuzivane pri pocitani velikosti parametru contextu, metoda vraci 0
    int getLengthForParams();
}
