package fjp.sp.simplegrammar.symbols;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Program;
import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.utils.Wrapper;

import java.util.HashMap;

public abstract class Context {

    //tabulka symbolu

    //symboly v tabulce
    protected HashMap<String, Symbol> symbols = new HashMap<>();

    //pocitani adres, adresy zaciname pocitat od velikosti AZ, tj prvni promenna bude mit adresu 3
    //pouze pro promenne, metody nemaji adresu v zasobniku
    private int address = Program.ACTIVATION_RECORD_SIZE;

    //vlozeni symbolu do tabulky
    public void insertSymbol(String name, Symbol symbol) {


        if(symbols.containsKey(name)) {
            ErrorHandler.getInstance().error("Symbol " + name + " already declared");
        }

        //spocitame adresu, promenne ji zvysi o svoji velikost, metody ne
        address = symbol.setAddress(address);
        symbols.put(name, symbol);
    }

    //ziskani typu
    public Type getType(String name) {
        //najdeme v tabulce symbol a vratime jeho typ
        Symbol symbol = getSymbol(name, new Wrapper<>(0));
        return symbol.getType();
    }

    //ziskani promenne pro jeji nastaveni (generovani instrukce STO)
    public VarDescriptor assign(String name, Wrapper<Integer> level) {
        //nalezeni symbolu v tabulce
        Symbol symbol = getSymbol(name, level);

        //deskriptor promenne vrati sam sebe, metoda zahlasi chybu, konstanta lze ulozit pouze 1x
        return symbol.varAssign();
    }

    //ziskani promenne pro jeji nacteni do zasobniku (generovani instrukce LOD)
    public VarDescriptor retrieve(String name, Wrapper<Integer> level) {
        //nalezeni symbolu v tabulce
        Symbol symbol = getSymbol(name, level);

        //deskriptor promenne vrati sam sebe, metoda zahlasi chybu
        return symbol.varRetrieve();
    }

    //ziskani descriptoru metody pro zavolani
    public MethodDescriptor call(String name) {
        //nalezeni symbolu v tabulce
        Symbol symbol = getSymbol(name, new Wrapper<>(0));

        //metoda vrati sama sebe, promenne zahlasi chybu
        return symbol.call();
    }

    //ziskani velikosti promennych v konkretnich contextu
    //vhodne pro INT na zacatku metody (delame INT 3 + paramsLength)
    public int getContextParamsLength() {
        int rvalue = 0;
        for(Symbol symbol : symbols.values()) {
            rvalue += symbol.getLengthForParams();
        }
        return rvalue;
    }

    //ziskani symbolu, definuje lokalni a globalni context
    protected abstract Symbol getSymbol(String name, Wrapper<Integer> level);

}
