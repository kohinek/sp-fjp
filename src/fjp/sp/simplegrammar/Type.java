package fjp.sp.simplegrammar;

import java.util.Arrays;

public enum Type {

    BOOLEAN, INTEGER, RATIO, VOID;

    //ziskani typu ze stringu
    public static Type getType(String sType) {
        switch(sType) {
            case "boolean":
                return BOOLEAN;
            case "int":
                return INTEGER;
            case "ratio":
                return RATIO;
            case "void":
                return VOID;
        }

        return null;
    }

    //ziskani delky promenne (ratio jsou dve cisla)
    public int getLength() {
        switch(this) {
            case INTEGER:
            case BOOLEAN:
                return 1;
            case RATIO:
                return 2;
        }

        return 0;
    }


    //vraceni indexu do tabulky pouzivane v modulu expression
    public int getIndex() {
        return Arrays.asList(Type.values()).indexOf(this);
    }

    //ziskani typu z indexu
    public static Type getFromIndex(int index) {
        return Type.values()[index];
    }

    //vraceni poctu
    public static int getCount() {
        return Type.values().length;
    }


}
