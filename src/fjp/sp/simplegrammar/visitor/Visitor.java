package fjp.sp.simplegrammar.visitor;

import fjp.sp.simplegrammar.actions.Program;
import fjp.sp.simplegrammar.grammar.GrammarBaseVisitor;
import fjp.sp.simplegrammar.grammar.GrammarParser;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.symbols.Context;
import fjp.sp.simplegrammar.symbols.GlobalContext;
import fjp.sp.simplegrammar.symbols.MethodReturnInfo;
import fjp.sp.simplegrammar.visitor.*;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.ArrayList;
import java.util.List;

public class Visitor extends GrammarBaseVisitor<Action> {

    //hlavni visitor programu
    //drzi odkaz na kontextualni / podrazene visitory

    //soucasti visitoru je tabulka symbolu (context) a informace o navratove hodnote
    //visitor je zvlast zavolan pri zpracovani kazde metody
    //odpovida tedy 1 metoda = 1 visitoru + visitor pro globalni context
    private Context context;
    private MethodReturnInfo methodReturnInfo;

    //odkazy na podrazene visitory
    private StatementVisitor statementVisitor;
    private MethodVisitor methodVisitor;
    private VarVisitor varVisitor;
    private ValueVisitor valueVisitor;
    private ExpressionVisitor expressionVisitor;

    //konstruktor, pokud vytvarime lokalni metodu
    public Visitor(Context context, MethodReturnInfo methodReturnInfo) {
        this.context = context;
        this.methodReturnInfo = methodReturnInfo;
    }

    //konstroktor pro globalni kontext
    public Visitor(Context context) {
        this.context = context;
        this.methodReturnInfo = null;
    }


    //pokud jsme visitor jeste nezalozili, udelame to, jinak vratime jiz zalozeny
    private StatementVisitor getStatementVisitor() {
        if(statementVisitor == null) {
            statementVisitor = new StatementVisitor(context, this, methodReturnInfo);
        }

        return statementVisitor;
    }

    private MethodVisitor getMethodVisitor() {
        if(methodVisitor == null) {
            methodVisitor = new MethodVisitor(context, this);
        }

        return methodVisitor;
    }

    private VarVisitor getVarVisitor() {
        if( varVisitor == null) {
            varVisitor = new VarVisitor(context, this);
        }

        return varVisitor;
    }

    private ValueVisitor getValueVisitor() {
        if(valueVisitor == null) {
            valueVisitor = new ValueVisitor(context, this);
        }

        return valueVisitor;
    }

    private ExpressionVisitor getExpressionVisitor() {
        if(expressionVisitor == null) {
            expressionVisitor = new ExpressionVisitor(context, this);
        }

        return expressionVisitor;
    }

    //navstivi body programu (viz gramatika)
    @Override
    public Action visitProgram(GrammarParser.ProgramContext ctx) {
        return super.visitProgram(ctx);
    }


    //zpracuje body programu
    @Override
    public Action visitBody(GrammarParser.BodyContext ctx) {
        //v programu mohou byt deklarovany globalni promenne nebo metody
        //nacteme tedy obe kategorie a ty pridame do akce program
        List<Action> varDeclarations = new ArrayList<>();
        List<Action> methods = new ArrayList<>();
        for(GrammarParser.VarDeclarationLineContext varDeclarationLineContext : ctx.varDeclarationLine()) {
            varDeclarations.add(this.visit(varDeclarationLineContext));
        }

        for(GrammarParser.MethodContext methodContext : ctx.method()) {
            methods.add(this.visit(methodContext));
        }

        return new Program(varDeclarations, methods, context);
    }


    //vsechny ostatni uzly hlavni visitor rozdeli mezi podrizene visitory

    @Override
    public Action visitBlockStatement(GrammarParser.BlockStatementContext ctx) {
        return getStatementVisitor().visit(ctx);
    }

    @Override
    public Action visitIfStatement(GrammarParser.IfStatementContext ctx) {
        return getStatementVisitor().visit(ctx);
    }

    @Override
    public Action visitForStatement(GrammarParser.ForStatementContext ctx) {
        return getStatementVisitor().visit(ctx);
    }

    @Override
    public Action visitWhileStatement(GrammarParser.WhileStatementContext ctx) {
        return getStatementVisitor().visit(ctx);
    }

    @Override
    public Action visitVarAssignmentStatement(GrammarParser.VarAssignmentStatementContext ctx) {
        return getStatementVisitor().visit(ctx);
    }

    @Override
    public Action visitVarDeclarationStatement(GrammarParser.VarDeclarationStatementContext ctx) {
        return getStatementVisitor().visit(ctx);
    }

    @Override
    public Action visitMethodCallStatement(GrammarParser.MethodCallStatementContext ctx) {
        return getStatementVisitor().visit(ctx);
    }

    @Override
    public Action visitVarDeclarationLine(GrammarParser.VarDeclarationLineContext ctx) {
        return getVarVisitor().visit(ctx);

    }


    @Override
    public Action visitVarAssignment(GrammarParser.VarAssignmentContext ctx) {
        return getVarVisitor().visit(ctx);
    }

    @Override
    public Action visitExpression(GrammarParser.ExpressionContext ctx) {
        return getExpressionVisitor().visit(ctx);
    }

    @Override
    public Action visitExpression1(GrammarParser.Expression1Context ctx) {
        return getExpressionVisitor().visit(ctx);
    }

    @Override
    public Action visitExpression2(GrammarParser.Expression2Context ctx) {
        return getExpressionVisitor().visit(ctx);
    }

    @Override
    public Action visitExpression3(GrammarParser.Expression3Context ctx) {
        return getExpressionVisitor().visit(ctx);
    }

    @Override
    public Action visitExpression4(GrammarParser.Expression4Context ctx) {
        return getExpressionVisitor().visit(ctx);
    }

    @Override
    public Action visitExpression5(GrammarParser.Expression5Context ctx) {
        return getExpressionVisitor().visit(ctx);
    }

    @Override
    public Action visitExpVal(GrammarParser.ExpValContext ctx) {
        return getExpressionVisitor().visit(ctx);
    }

    @Override
    public Action visitExpIdent(GrammarParser.ExpIdentContext ctx) {
        return getExpressionVisitor().visit(ctx);
    }

    @Override
    public Action visitExpMethod(GrammarParser.ExpMethodContext ctx) {
        return getExpressionVisitor().visit(ctx);
    }

    @Override
    public Action visitMethodCall(GrammarParser.MethodCallContext ctx) {
        return getMethodVisitor().visit(ctx);
    }

    @Override
    public Action visitMethod(GrammarParser.MethodContext ctx) {
        return getMethodVisitor().visit(ctx);
    }

    @Override
    public Action visitVal(GrammarParser.ValContext ctx) {
        return getValueVisitor().visit(ctx);
    }

    @Override
    public Action visitDecimal(GrammarParser.DecimalContext ctx) {
        return getValueVisitor().visit(ctx);
    }

    @Override
    public Action visitReturnStatement(GrammarParser.ReturnStatementContext ctx) {
        return getStatementVisitor().visit(ctx);
    }

    @Override
    public Action visitReturnVoidStatement(GrammarParser.ReturnVoidStatementContext ctx) {
        return getStatementVisitor().visit(ctx);
    }
}
