package fjp.sp.simplegrammar.visitor;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.methods.MethodCall;
import fjp.sp.simplegrammar.actions.methods.MethodDeclaration;
import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.grammar.GrammarBaseVisitor;
import fjp.sp.simplegrammar.grammar.GrammarParser;
import fjp.sp.simplegrammar.grammar.GrammarVisitor;
import fjp.sp.simplegrammar.symbols.*;

import java.util.ArrayList;
import java.util.List;

public class MethodVisitor extends GrammarBaseVisitor<Action> {

    //visitor slouzici k zpracovani volani a deklaraci metod


    //odkaz na tabulku symbolu
    private Context context;

    //odkaz na hlavni visitor(tomu zpetne predava veskere navstevy uzlu)
    private GrammarVisitor<Action> parent;


    public MethodVisitor(Context context, GrammarBaseVisitor<Action> parent) {
        this.context = context;
        this.parent = parent;
    }


    //zpracovani volani metody
    @Override
    public Action visitMethodCall(GrammarParser.MethodCallContext ctx) {
        //nazev metody
        String identifier = ctx.IDENT().getText();

        //parametry metody
        List<Action> expressions = new ArrayList<>();

        //zpracujeme jednotlive parametry
        for(GrammarParser.ExpressionContext context : ctx.expression()) {
            expressions.add(parent.visit(context));
        }

        //vytvoreni akce v AST
        return new MethodCall(expressions, identifier, this.context);

    }

    @Override
    public Action visitMethod(GrammarParser.MethodContext ctx) {
        //pokud nam pocet typu a pocet promennych neodpovida, je chyba bud v gramatice nebo antlr
        //(jeste se nam v praxi ale chyba visitoru nestala)
        if(ctx.TYPE().size() != ctx.IDENT().size()) {
            ErrorHandler.getInstance().error("Chyba visitoru");
        }

        //vstupujeme do metody, budeme tedy menit tabulku smybolu (a hlavni visitor, ktery ji drzi)
        LocalContext localContext = new LocalContext(context);

        //nazev metody
        String identifier = ctx.IDENT(0).getText();

        //navratova hodnota
        Type type = Type.getType(ctx.TYPE(0).getText());

        //velikost vstupnich parametru metody (spocitame nize)
        int paramSize = 0;

        //zpracovani parametru
        //prvni typ a identifikator patri navratove hodnote a jmenu metody (viz gramatika)
        //zacneme tedy od 1
        for(int i = 1; i < ctx.TYPE().size(); i++) {
            //nazev parametru
            String parId = ctx.IDENT(i).getText();

            //typ parametru
            Type parType = Type.getType(ctx.TYPE(i).getText());
            assert parType != null;

            //pridani do tabulky symbolu
            localContext.insertSymbol(parId, new VarDescriptor(parType));

            //pripocteme velikost
            paramSize += parType.getLength();
        }

        //informace o navratove hodnote a velikosti parametru
        //paramsize slouzi k informaci, abychom vedeli, na kterou adresu mame ulozit navratovou hodnotu
        MethodReturnInfo info = new MethodReturnInfo(paramSize, type);

        //zmena visitoru
        Visitor localContextVisitor = new Visitor(localContext, info);

        //zpracovani statementu metody (typicky block statement)
        Action statement = localContextVisitor.visit(ctx.statement());

        //overeni, ze funkce ma return, return musi byt posledni instrukce metody
        if(!statement.hasReturn()) {
            ErrorHandler.getInstance().error("Method " + identifier + " does not contain return statement.");
        }

        //akce pro AST
        return new MethodDeclaration(type, identifier, statement, context, localContext, paramSize);



    }
}
