package fjp.sp.simplegrammar.visitor;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.values.ValueRetrievalBoolean;
import fjp.sp.simplegrammar.actions.values.ValueRetrievalInt;
import fjp.sp.simplegrammar.actions.values.ValueRetrievalRatio;
import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.grammar.GrammarBaseVisitor;
import fjp.sp.simplegrammar.grammar.GrammarParser;
import fjp.sp.simplegrammar.grammar.GrammarVisitor;
import fjp.sp.simplegrammar.symbols.Context;

public class ValueVisitor extends GrammarBaseVisitor<Action> {

    //zpracovani hodnot v programu

    //viz jine visitory
    private Context context;
    private GrammarVisitor<Action> parent;

    public ValueVisitor(Context context, GrammarBaseVisitor<Action> parent) {
        this.context = context;
        this.parent = parent;
    }

    //zpracovani desetinneho cisla
    @Override
    public Action visitDecimal(GrammarParser.DecimalContext ctx) {
        return new ValueRetrievalRatio(ctx.INT(0).getText(), ctx.INT(1).getText());
    }

    //zpracovani hodnoty
    @Override
    public Action visitVal(GrammarParser.ValContext ctx) {

        //zpracovani intu, pridame akci pro AST
        if(ctx.INT() != null) {
            return new ValueRetrievalInt(ctx.INT().getText());
        }

        //zpracovani desetinneho cisla, postara se o to metoda nahore
        if(ctx.decimal() != null) {
            return parent.visit(ctx.decimal());
        }

        //zpracovani intu, vratime akci pro AST
        if(ctx.BOOLEAN() != null) {
            return new ValueRetrievalBoolean(ctx.BOOLEAN().getText());
        }

        //nestane se, jinak chyba antlr
        ErrorHandler.getInstance().error("Chyba visitoru");

        return null;
    }


}
