package fjp.sp.simplegrammar.visitor;

import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.NullAction;
import fjp.sp.simplegrammar.actions.statements.*;
import fjp.sp.simplegrammar.grammar.GrammarBaseVisitor;
import fjp.sp.simplegrammar.grammar.GrammarParser;
import fjp.sp.simplegrammar.grammar.GrammarVisitor;
import fjp.sp.simplegrammar.symbols.Context;
import fjp.sp.simplegrammar.symbols.MethodReturnInfo;

import java.util.ArrayList;
import java.util.List;

public class StatementVisitor extends GrammarBaseVisitor<Action> {

    //tabulka symbolu
    private Context context;

    //odkaz na rodice
    private GrammarVisitor<Action> parent;

    //informace o navratove hodnote, kterou potrebuje akce return statement
    private MethodReturnInfo methodReturnInfo;

    public StatementVisitor(Context context, GrammarBaseVisitor<Action> parent, MethodReturnInfo methodReturnInfo) {
        this.context = context;
        this.methodReturnInfo = methodReturnInfo;
        this.parent = parent;
    }

    

    @Override
    public Action visitBlockStatement(GrammarParser.BlockStatementContext ctx) {
        //zpracovani bloku prikazu (v {})
        List<Action> statements = new ArrayList<>();

        //zpracujeme kazdy prikaz zvlast a vratime do seznamu
        for(GrammarParser.StatementContext statementContext : ctx.statement()) {
            statements.add(parent.visit(statementContext));
        }

        //akce pro AST
        return new BlockStatement(statements, context);
    }

    //zpracovani ifu
    @Override
    public Action visitIfStatement(GrammarParser.IfStatementContext ctx) {
        //expression uvnitr ifu (if (expression) statement)
        Action expression = parent.visit(ctx.expression());

        Action trueStatement; //statement, kdyz if pravda
        Action falseStatement; //statement pro else

        //seznam statementu (1 pro if bez elsu, 2 pro if s elsem)
        List<GrammarParser.StatementContext> list = ctx.statement();
        trueStatement = parent.visit(list.get(0));

        //pokud je if s elsem, pridame false statement
        if(list.size() == 1) {
            falseStatement = new NullAction();
        } else {
            falseStatement = parent.visit(list.get(1));
        }

        //akce pro AST
        return new IfStatement(expression, trueStatement, falseStatement);
    }

    //zpracovani foru
    @Override
    public Action visitForStatement(GrammarParser.ForStatementContext ctx) {
        Action before; //provedeme pred forem (typicky for(int i = 0) ...
        Action after; //provedeme na konci kazde iterace (typicky i = i + 1)

        List<GrammarParser.VarAssignmentContext> list = ctx.varAssignment();

        if(list.size() == 1) {
            //pokud mame jen 1 assignment, znamena to, ze deklarujeme promenne pred forem
            before = parent.visit(ctx.varDeclarationLine());
            after = parent.visit(list.get(0));
        } else {
            //pred forem pouze prirazujeme hodnotu
            before = parent.visit(list.get(0));
            after = parent.visit(list.get(1));
        }

        //vyhodnoceni podminky foru
        Action expression = parent.visit(ctx.expression());

        //prikaz (blok prikazu), ktery for provede ve sve iteraci
        Action statement = parent.visit(ctx.statement());

        //akce pro AST
        return new ForStatement(before, expression, after, statement);
    }

    //zpracovani whilu
    @Override
    public Action visitWhileStatement(GrammarParser.WhileStatementContext ctx) {
        //ziskani podminky
        Action expression = parent.visit(ctx.expression());

        //ziskani prikazu pro iteraci
        Action statement = parent.visit(ctx.statement());

        //akce pro AST
        return new WhileStatement(expression, statement);
    }

    @Override
    public Action visitVarAssignmentStatement(GrammarParser.VarAssignmentStatementContext ctx) {
        //prirazujeme promenou, zpracujeme ve svem uzlu
        return parent.visit(ctx.varAssignment());
    }

    @Override
    public Action visitVarDeclarationStatement(GrammarParser.VarDeclarationStatementContext ctx) {
        //deklarujeme promenne, zpracujeme ve svem uzlu
        return parent.visit(ctx.varDeclarationLine());
    }

    @Override
    public Action visitMethodCallStatement(GrammarParser.MethodCallStatementContext ctx) {
        //volame metodu, zpracujeme ve svem uzlu
        return parent.visit(ctx.methodCall());
    }

    @Override
    public Action visitReturnStatement(GrammarParser.ReturnStatementContext ctx) {
        //skaceme pryc z metody, obsahuje expression
        //nacteme expression
        Action expression = parent.visit(ctx.expression());

        //a vratime akci pro AST
        return new ReturnStatement(expression, methodReturnInfo);
    }

    //return pro void
    @Override
    public Action visitReturnVoidStatement(GrammarParser.ReturnVoidStatementContext ctx) {
        return new ReturnStatement(methodReturnInfo);
    }
}
