package fjp.sp.simplegrammar.visitor;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.grammar.GrammarBaseVisitor;
import fjp.sp.simplegrammar.grammar.GrammarParser;
import fjp.sp.simplegrammar.grammar.GrammarVisitor;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.values.VarAssignment;
import fjp.sp.simplegrammar.actions.values.VarDeclaration;
import fjp.sp.simplegrammar.actions.values.VarDeclarationLine;
import fjp.sp.simplegrammar.symbols.Context;
import org.antlr.v4.runtime.tree.TerminalNode;

public class VarVisitor extends GrammarBaseVisitor<Action> {

    //zpracovani promennych

    //viz jine visitory
    private Context context;
    private GrammarVisitor<Action> parent;

    public VarVisitor(Context context, GrammarBaseVisitor<Action> parent) {
        this.context = context;
        this.parent = parent;
    }

    //zpracovani radku s deklaracemi
    @Override
    public Action visitVarDeclarationLine(GrammarParser.VarDeclarationLineContext ctx) {
        //typ deklarace, 1 pro celou radku
        String sType = ctx.TYPE().getText();
        Type type = Type.getType(sType);
        VarDeclarationLine varDeclarationLine = new VarDeclarationLine(type);
        boolean constant = ctx.CONST() != null;

        //line se sklada z n deklaraci
        //tento for je proiteruje
        for(GrammarParser.VarDeclarationContext context : ctx.varDeclaration()) {
            addVarDeclarationChildren(context, varDeclarationLine, constant);
        }

        //vratime akci pro ast
        return varDeclarationLine;
    }

    private void addVarDeclarationChildren(GrammarParser.VarDeclarationContext ctx, VarDeclarationLine line, boolean constant) {

        //v gramatice je povolene k var deklaraci zaroven pridat expression, ktery rovnou nastavi hodnotu promenne
        GrammarParser.ExpressionContext expressionContext = ctx.expression();

        //proiterovani identifikatoru (pokud mame a = b = 2, je jich vice)
        for(TerminalNode node : ctx.IDENT()) {
            //nazev promenne
            String variableName = node.getSymbol().getText();

            //pridani deklaraci k akci v AST
            line.addDeclaration(new VarDeclaration(variableName, line.getType(), context, constant));

            //pokud zaroven deklarace obsahuje assignment, pridame expression k akci AST
            if(expressionContext != null) {
                VarAssignment assignment = new VarAssignment(variableName, parent.visit(ctx.expression()), this.context);
                line.addAssignment(assignment);
            }

        }

    }

    //zpracovani prirazeni promenne
    @Override
    public Action visitVarAssignment(GrammarParser.VarAssignmentContext ctx) {
        //hodnota
        Action expression = parent.visit(ctx.expression());

        //identifikator
        String identifier = ctx.IDENT().getText();

        //vratime akci pro AST
        return new VarAssignment(identifier, expression, this.context);
    }
}
