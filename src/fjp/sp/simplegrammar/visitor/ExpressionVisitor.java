package fjp.sp.simplegrammar.visitor;


import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.*;
import fjp.sp.simplegrammar.actions.values.VarRetrieval;
import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.grammar.GrammarBaseVisitor;
import fjp.sp.simplegrammar.grammar.GrammarParser;
import fjp.sp.simplegrammar.grammar.GrammarVisitor;
import fjp.sp.simplegrammar.symbols.Context;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTree;

public class ExpressionVisitor extends GrammarBaseVisitor<Action> {

    //visitor slouzici ke zpracovani expressions

    //odkaz na tabulku symbolu
    private Context context;

    //odkaz na hlavni visitor(tomu zpetne predava veskere navstevy uzlu)
    private GrammarVisitor<Action> parent;

    public ExpressionVisitor(Context context, GrammarBaseVisitor<Action> parent) {
        this.context = context;
        this.parent = parent;
    }

    //zpracovani vsech uzlu expression 0-3, drzi se stejneho schematu
    private Action getExpression(ParserRuleContext ctx, ParserRuleContext left, ParserRuleContext right, Token op) {
        if(ctx.children.size() == 1) { //pokud ma uzel jen jednoho potomka, zpracujeme ho
            return parent.visit(right);
        } else {

            //pokud ma potomky 3, zpracujeme nejdriv leveho, pak praveho a podle operatoru vytvorime spravnou expression
            Action actionLeft = parent.visit(left);
            Action actionRight = parent.visit(right);
            Action rvalue = ExpressionFactory.getExpressionByTitle(op.getText(), actionLeft, actionRight);


            if(rvalue == null) {
                ErrorHandler.getInstance().error("Chyba visitoru");
            }
            return rvalue;

        }
    }

    //expressiony ve formatu E -> E + T | T zpracujeme tak, ze je predame metode nahore
    @Override
    public Action visitExpression(GrammarParser.ExpressionContext ctx)  {
        return getExpression(ctx, ctx.expression(), ctx.expression1(), ctx.op);
    }

    @Override
    public Action visitExpression1(GrammarParser.Expression1Context ctx) {
        return getExpression(ctx, ctx.expression1(), ctx.expression2(), ctx.op);
    }

    @Override
    public Action visitExpression2(GrammarParser.Expression2Context ctx) {
        return getExpression(ctx, ctx.expression2(), ctx.expression3(), ctx.op);
    }

    @Override
    public Action visitExpression3(GrammarParser.Expression3Context ctx) {
        return getExpression(ctx, ctx.expression3(), ctx.expression4(), ctx.op);
    }


    //expression 4 obsahuje zpracovani unarnich operatoru E -> +E | E
    @Override
    public Action visitExpression4(GrammarParser.Expression4Context ctx) {
        if(ctx.children.size() == 1) {
            return parent.visit(ctx.expression5());
        } else {
            //pokud obsahuje akci tak navstivime a podle operatoru pridame expression
            Action right = parent.visit(ctx.expression5());
            Action rvalue = ExpressionFactory.getExpressionByTitle(ctx.op.getText(), right);

            if(rvalue == null) {
                ErrorHandler.getInstance().error("Chyba visitoru");
            }

            return rvalue;
        }
    }


    //zpracovani zavorek
    @Override
    public Action visitExpression5(GrammarParser.Expression5Context ctx) {
        if(ctx.children.size() == 1) {
            return parent.visit(ctx.expression6());
        } else {
            return parent.visit(ctx.expression());
        }
    }

    //pokud je expression hodnota, predame zpracovani uzlu, ktery hodnotu rozpozna
    @Override
    public Action visitExpVal(GrammarParser.ExpValContext ctx) {
       return parent.visit(ctx.val());
    }

    //pokud chceme pouzit identifikator, pridame akci, ktera slouzi k ziskani hodnoty promenne
    @Override
    public Action visitExpIdent(GrammarParser.ExpIdentContext ctx) {
        return new VarRetrieval(ctx.IDENT().getText(), this.context);
    }

    //pokud chceme pouzit volani metody, predame zpracovani uzlu, ktery ji zavola
    @Override
    public Action visitExpMethod(GrammarParser.ExpMethodContext ctx) {
        return parent.visit(ctx.methodCall());
    }
}
