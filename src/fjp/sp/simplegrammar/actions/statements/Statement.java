package fjp.sp.simplegrammar.actions.statements;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;

public abstract class Statement extends Action {

    //statement vraci void

    @Override
    public Type getType() {
        return Type.VOID;
    }
}
