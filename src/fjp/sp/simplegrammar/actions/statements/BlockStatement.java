package fjp.sp.simplegrammar.actions.statements;

import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.instructions.Instruction;
import fjp.sp.simplegrammar.symbols.Context;

import java.util.List;

public class BlockStatement extends Statement {

    //prikazy za sebou ohranicene { }

    //prikazy
    private List<Action> statements;

    //konstruktor
    public BlockStatement(List<Action> statements, Context context) {
        this.statements = statements;
        this.context = context;
    }

    @Override
    public void generate(List<Instruction> instructions) {

        //pri generovani pouze postupne vygenerujeme ostatni prikazy
        for(Action statement : statements) {
            statement.generate(instructions);
        }
    }

    @Override
    public boolean hasReturn() {
        //pokud je posledni polozka return, tak ok
        return statements.get(statements.size() - 1) instanceof ReturnStatement;
    }
}
