package fjp.sp.simplegrammar.actions.statements;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.instructions.Instruction;
import fjp.sp.simplegrammar.instructions.InstructionType;
import fjp.sp.simplegrammar.instructions.Label;
import fjp.sp.simplegrammar.instructions.PL0Instruction;

import java.util.List;

public class ForStatement extends Statement {

    //for statement

    //akce pred (typicky int i = )
    private Action beforeFor;

    //podminka za ktere cyklus bezi (i < length)
    private Action condition;

    //prirazeni po dobehnuti iterace (i = i + 1)
    private Action varAssignment;

    //vlastni kod iterace
    private Action statement;

    public ForStatement(Action beforeFor, Action condition, Action varAssignment, Action statement) {
        this.beforeFor = beforeFor;
        this.condition = condition;
        this.varAssignment = varAssignment;
        this.statement = statement;
    }

    @Override
    public void generate(List<Instruction> instructions) {
        //expression, ktery generujeme musi byt boolean
        if(condition.getType() != Type.BOOLEAN) {
            ErrorHandler.getInstance().error("For condition must be boolean type");
        }

        //vygenerujeme si labely pro skok na zacatek a konec for cyklu
        Label start = new Label();
        Label end = new Label();

        //nejdrive vygenerujeme cast pred for cyklem
        beforeFor.generate(instructions);

        //pote vygenerujeme kod podminky
        //na podminku budeme skakat pri konci iterace
        instructions.add(start);
        condition.generate(instructions);
        //pokud podminka neni splnena, skocime na konec for cyklu
        instructions.add(new PL0Instruction(
                InstructionType.JMC,
                end
        ));

        //kod iterace
        statement.generate(instructions);
        //prirazeni je na konci
        varAssignment.generate(instructions);
        //skok na podminku
        instructions.add(new PL0Instruction(
                InstructionType.JMP,
                start
        ));

        //label s koncem
        instructions.add(end);
    }
}
