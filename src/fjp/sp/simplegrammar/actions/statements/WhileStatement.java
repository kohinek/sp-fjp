package fjp.sp.simplegrammar.actions.statements;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.instructions.*;

import java.util.List;

public class WhileStatement extends Statement {

    //prikaz while

    //podminka
    private Action expression;
    //kod iterace
    private Action statement;

    public WhileStatement(Action expression, Action statement) {
        this.expression = expression;
        this.statement = statement;
    }


    @Override
    public void generate(List<Instruction> instructions) {
        //expression, ktery generujeme musi byt boolean
        if(expression.getType() != Type.BOOLEAN) {
            ErrorHandler.getInstance().error("While condition must be boolean type");
        }


        //labely odkazujici na start a konec
        Label start = new Label();
        Label end = new Label();


        //sem budeme skakat po kazde iteraci
        instructions.add(start);

        //vygenerujeme expression
        expression.generate(instructions);

        //pokud podminka neprosla, skaceme na konec
        instructions.add(new PL0Instruction(
                InstructionType.JMC,
                end
        ));

        //vygenerujeme kod iterace
        statement.generate(instructions);
        instructions.add(new PL0Instruction(
                InstructionType.JMP,
                start
        ));

        //skok na konec
        instructions.add(end);


    }
}
