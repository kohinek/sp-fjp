package fjp.sp.simplegrammar.actions.statements;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.values.VarConversion;
import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.instructions.Instruction;
import fjp.sp.simplegrammar.instructions.InstructionWOLibcall;
import fjp.sp.simplegrammar.instructions.InstructionType;
import fjp.sp.simplegrammar.instructions.PL0Instruction;
import fjp.sp.simplegrammar.symbols.MethodReturnInfo;

import java.util.List;

public class ReturnStatement extends Statement {

    //prikaz obsahujici return

    //budeme rozlisovat pripady, kdy se vracime z voidove a nevoidove metody
    private boolean voidReturn;

    //hodnota, kterou vracime
    private Action expression;

    //informaci o returnu metody (ziskame z ni typ navratove hodnoty a adresu, kam ji ulozime)
    private MethodReturnInfo methodReturnInfo;

    public ReturnStatement(MethodReturnInfo methodReturnInfo) {
        if(methodReturnInfo.methodType != Type.VOID) {
            ErrorHandler.getInstance().error("Void return for non-void method.");
        }

        this.voidReturn = true;
    }

    public ReturnStatement(Action expression, MethodReturnInfo methodReturnInfo) {
        this.voidReturn = false;
        this.expression = expression;
        this.methodReturnInfo = methodReturnInfo;
    }

    @Override
    public void generate(List<Instruction> instructions) {

        //pokud je void, vygenerujeme void return
        if(voidReturn) {
            generateVoidReturn(instructions);
            //pokud neni void, vygenerujeme non void return
        } else {
            generateNonVoidReturn(instructions);
        }
    }

    private void generateVoidReturn(List<Instruction> instructions) {
        //pri voidu akorat pridame instrukce RET
        instructions.add(new PL0Instruction(InstructionType.RET, 0));
    }

    private void generateNonVoidReturn(List<Instruction> instructions) {
        //pri non voidu vygenerujeme hodnotu expressionu
        expression.generate(instructions);

        //prevedeme ho na navratovou hodnotu metody
        VarConversion varConversion = new VarConversion(methodReturnInfo.methodType, expression.getType());
        varConversion.generate(instructions);

        //a vygenerujeme instrukce STO -> nacteni navratove hodnoty a metodu vyse
        int returnValueBase = -1 * (methodReturnInfo.paramSize + methodReturnInfo.methodType.getLength());
        for(int i = methodReturnInfo.methodType.getLength() - 1; i >= 0; i--) {
            instructions.add(new PL0Instruction(InstructionType.STO, returnValueBase + i));
        }


        //nakonec instrukce RET
        instructions.add(new PL0Instruction(InstructionType.RET, 0));
    }

}
