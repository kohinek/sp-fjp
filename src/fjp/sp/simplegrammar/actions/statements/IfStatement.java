package fjp.sp.simplegrammar.actions.statements;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.instructions.*;

import java.util.List;

public class IfStatement extends Statement {

    //prikaz if

    //vyhodnoceni podminky
    private Action expression;
    //statement pro if true
    private Action trueStatement;
    //statement pro else
    private Action falseStatement;

    public IfStatement(Action expression, Action trueStatement, Action falseStatement) {
        this.expression = expression;
        this.trueStatement = trueStatement;
        this.falseStatement = falseStatement;
    }

    @Override
    public void generate(List<Instruction> instructions) {
        //podminka musi byt boolean
        if(expression.getType() != Type.BOOLEAN) {
            ErrorHandler.getInstance().error("If expression must by boolean type");
        }

        //vygenerujeme si labely pro skok na konec a do else vetve
        Label end = new Label();
        Label falseS = new Label();

        //vygenerujeme podminku
        expression.generate(instructions);

        //pokud nesplnena, skocime do else, jinak pokracujeme
        instructions.add(new PL0Instruction(
                InstructionType.JMC,
                falseS
        ));

        //vygenerujeme true statement a skocime na konec, else provadet nebudeme
        trueStatement.generate(instructions);
        instructions.add(new PL0Instruction(
                InstructionType.JMP,
                end
        ));

        //vygenerujeme else vetev s labelem
        instructions.add(falseS);
        falseStatement.generate(instructions);

        //label s koncem
        instructions.add(end);


    }
}
