package fjp.sp.simplegrammar.actions;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.instructions.Instruction;

import java.util.List;

public class NullAction extends Action {

    //prazdna akce
    //null object

    @Override
    public void generate(List<Instruction> instructions) {
    }

    @Override
    public Type getType() {
        return Type.VOID;
    }
}
