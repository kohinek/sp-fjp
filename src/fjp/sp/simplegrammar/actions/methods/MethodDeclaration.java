package fjp.sp.simplegrammar.actions.methods;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.Program;
import fjp.sp.simplegrammar.instructions.*;
import fjp.sp.simplegrammar.symbols.*;

import java.util.List;

public class MethodDeclaration extends Action {

    //deklarace metody

    //navratova hodnota
    private Type returnType;
    //identifikator
    private String methodName;
    //prikaz (blok prikazu) metody
    private Action statement;
    //zaznam v tabulce symbolu
    private MethodDescriptor descriptor;
    //odkaz na lokalni kontext tabulky symbolu
    private LocalContext localContext;
    //velikost vstupnich parametru metody call(int a, ratio b) = 1 + 2 = 3
    private int paramSize;

    public MethodDeclaration(Type returnType, String methodName,  Action statement, Context parentContext, LocalContext localContext, int methodParamSize) {
        this.returnType = returnType;
        this.methodName = methodName;
        this.statement = statement;
        this.context = parentContext;
        this.localContext = localContext;
        this.paramSize = methodParamSize;

        addSymbolToTable();
    }

    private void addSymbolToTable() {

        //pridame symbol pro metodu do globalniho kontextu
        this.descriptor = new MethodDescriptor(returnType);
        this.context.insertSymbol(methodName, descriptor);
    }

    @Override
    public void generate(List<Instruction> instructions) {


        //na zacatek pridame label, na ktery budou moc method cally skakat
        instructions.add(descriptor.getLabel());


        //nacteme misto pro AZ
        instructions.add(new PL0Instruction(
                InstructionType.INT,
                0,
                new InstructionIntegerWrapper(Program.ACTIVATION_RECORD_SIZE)
        ));

        //nacteme N parametru metody
        for(int i = paramSize; i > 0; i--) {
            instructions.add(new PL0Instruction(
                    InstructionType.LOD,
                    0,
                    new InstructionIntegerWrapper(-1 * i)
            ));
        }

        //v local contextu (metoda getContextParamLength) jsou dohromady zapocitane parametry metody i parametry definovane uvnitr metody
        //parametry metody jsme si nacetli o nekolik radek nize, cili vytvorime prostor pouze pro zbyle parametry
        instructions.add(new PL0Instruction(
                InstructionType.INT,
                0,
                new InstructionIntegerWrapper(localContext.getContextParamsLength() - paramSize)
        ));



        //vygenerujeme instrukce metody
        //o return se stara return statement
        statement.generate(instructions);

    }


    @Override
    public Type getType() {
        return returnType;
    }
}
