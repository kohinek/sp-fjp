package fjp.sp.simplegrammar.actions.methods;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.instructions.*;
import fjp.sp.simplegrammar.symbols.Context;
import fjp.sp.simplegrammar.symbols.MethodDescriptor;
import fjp.sp.simplegrammar.utils.Wrapper;

import java.util.List;

public class MethodCall extends Action {

    //akce AST pro volani metody

    //parametry metody
    private List<Action> expressions;
    //jmeno metody
    private String identifier;

    public MethodCall(List<Action> expressions, String identifier, Context context) {
        this.expressions = expressions;
        this.identifier = identifier;
        this.context = context;
    }

    @Override
    public void generate(List<Instruction> instructions) {

        //ziskame deskriptor metody z tabulky symbolu
        MethodDescriptor descriptor = this.context.call(identifier);

        //alokace pro navratovou hodnotu metody
        int length = descriptor.getType().getLength();
        if(length != 0) {
            instructions.add(new PL0Instruction(InstructionType.INT, length));
        }


        //vygenerujeme parametry
        int parametersSize = 0;
        for(Action action : expressions) {
            action.generate(instructions);
            parametersSize += action.getLength();
        }


        //zavolame podprogram
        instructions.add(new PL0Instruction(
                InstructionType.CAL,
                1,
                descriptor.getLabel()));

        //odstranime ze zasobniku promenne pro metodu
        instructions.add(new PL0Instruction(
                InstructionType.INT,
                0,
                new InstructionIntegerWrapper(-1 * parametersSize)));
    }

    @Override
    public Type getType() {
        return this.context.getType(identifier);
    }
}
