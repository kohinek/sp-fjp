package fjp.sp.simplegrammar.actions.values;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.instructions.Instruction;

import java.util.ArrayList;
import java.util.List;

public class VarDeclarationLine extends Action {

    //akce v AST reprezentujici celou radku deklaraci

    //typ
    private Type type;

    //muze obsahovat n deklaraci a n prirazeni
    private List<Action> declarations;
    private List<Action> assignments;

    public VarDeclarationLine(Type type) {
        this.type = type;
        declarations = new ArrayList<>();
        assignments = new ArrayList<>();
    }

    public void addDeclaration(Action declaration) {
        declarations.add(declaration);
    }

    public void addAssignment(Action assignment) {
        assignments.add(assignment);
    }


    @Override
    public void generate(List<Instruction> instructions) {

        //vygenerujeme nejdrive deklarace
        for(Action action : declarations) {
            action.generate(instructions);
        }

        //pote prirazeni
        for(Action action : assignments) {
            action.generate(instructions);
        }

    }

    @Override
    public Type getType() {
        return type;
    }
}
