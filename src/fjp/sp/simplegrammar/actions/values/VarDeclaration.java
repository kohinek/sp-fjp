package fjp.sp.simplegrammar.actions.values;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.instructions.Instruction;
import fjp.sp.simplegrammar.symbols.ConstDescriptor;
import fjp.sp.simplegrammar.symbols.Context;
import fjp.sp.simplegrammar.symbols.VarDescriptor;

import java.util.List;

public class VarDeclaration extends Action {

    //v AST deklaraci promenne

    //jmeno a typ
    private String variableName;
    private Type type;

    public VarDeclaration(String variableName, Type type, Context context, boolean constant) {
        this.variableName = variableName;
        this.type = type;
        this.context = context;

        addSymbolToTable(constant);
    }

    //pridame promennou do tabulky symbolu
    private void addSymbolToTable(boolean constant) {
        VarDescriptor descriptor = constant ? new ConstDescriptor(type) : new VarDescriptor(type);
        this.context.insertSymbol(variableName, descriptor);
    }

    @Override
    public void generate(List<Instruction> instructions) {
        //negenerujeme instrukce pri var deklaraci, misto pro ni se zalozi:
        //pro globalni promenne pri startu programu
        //pro lokalni promenne pri generovani metody
    }

    @Override
    public Type getType() {
        return type;
    }

}
