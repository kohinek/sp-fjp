package fjp.sp.simplegrammar.actions.values;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.instructions.*;

import java.util.List;

public class ValueRetrievalInt extends Action {

    //integer napsany jako cislo nacte akce do zasobniku

    private String value;

    //cislo je v kodu ulozene jako string
    public ValueRetrievalInt(String value)  {
        this.value = value;
    }

    @Override
    public void generate(List<Instruction> instructions) {
        //prevod je bezpecny, gramatika by maximalne umoznila pouze preteceni hodnoty cisla
        int value = Integer.parseInt(this.value);
        instructions.add(new PL0Instruction(InstructionType.LIT, value));
    }



    @Override
    public Type getType() {
        return Type.INTEGER;
    }
}
