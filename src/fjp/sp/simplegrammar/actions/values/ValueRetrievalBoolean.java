package fjp.sp.simplegrammar.actions.values;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.instructions.Instruction;
import fjp.sp.simplegrammar.instructions.InstructionType;
import fjp.sp.simplegrammar.instructions.PL0Instruction;

import java.util.List;

public class ValueRetrievalBoolean extends Action {

    //akce, ktera boolean napsany jako true/false naccte do zasobniku

    private String value;

    //zalozime ji pomoci stringove hodnoty true/false
    public ValueRetrievalBoolean(String value)  {
        this.value = value;
    }


    @Override
    public void generate(List<Instruction> instructions) {
        //hodnota je jen true/false, o to se postara gramtika
        if(this.value.equals("true")) {
            instructions.add(new PL0Instruction(InstructionType.LIT, 1));
        } else {
            instructions.add(new PL0Instruction(InstructionType.LIT, 0));
        }
    }

    @Override
    public Type getType() {
        return Type.BOOLEAN;
    }
}
