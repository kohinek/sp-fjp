package fjp.sp.simplegrammar.actions.values;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.conversions.*;
import fjp.sp.simplegrammar.instructions.Instruction;

import java.util.List;

public class VarConversion extends Action {

    //tabulka konverzi
    //na leve strane mame typ promenne, do ktere ukladame
    //na prave strane mame typ vyrazu
    private static Conversion[][] table;
    private Conversion conversion;

    static{
        int count = Type.getCount();
        table = new Conversion[count][count];

        //defaultne nepovolime zadnou konverzi
        for(int i = 0; i < count; i++) {
            for(int j = 0; j < count; j++) {
                table[i][j] = new InvalidConversion(Type.getFromIndex(j), Type.getFromIndex(i));
            }
        }

        //pokud mam stejny typ na prave i leve strane, neprovedu zadnou konverzi a povolim ulozeni
        table[Type.INTEGER.getIndex()][Type.INTEGER.getIndex()] = new EmptyConversion();
        table[Type.RATIO.getIndex()][Type.RATIO.getIndex()] = new EmptyConversion();
        table[Type.BOOLEAN.getIndex()][Type.BOOLEAN.getIndex()] = new EmptyConversion();

        //dalsi povolene konverze jsou INT -> RATIO a RATIO -> INT
        table[Type.INTEGER.getIndex()][Type.RATIO.getIndex()] = new RatioIntConversion();
        table[Type.RATIO.getIndex()][Type.INTEGER.getIndex()] = new IntRatioConversion();
    }

    private Type to;

    public VarConversion(Type to, Type from) {
        this.to = to;
        this.conversion = table[to.getIndex()][from.getIndex()];
    }

    @Override
    public void generate(List<Instruction> instructions) {
        this.conversion.generate(instructions);
    }

    @Override
    public Type getType() {
        return to;
    }
}
