package fjp.sp.simplegrammar.actions.values;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.instructions.Instruction;
import fjp.sp.simplegrammar.instructions.InstructionType;
import fjp.sp.simplegrammar.instructions.LibraryCall;
import fjp.sp.simplegrammar.instructions.PL0Instruction;
import fjp.sp.simplegrammar.utils.Wrapper;

import java.util.List;

public class ValueRetrievalRatio extends Action {

    //nacteni desetinneho cisla do zasobniku

    private String first;
    private String second;

    public ValueRetrievalRatio(String first, String second) {
        this.first = first;
        this.second = second;
    }

    private static final int[] POWERS_OF_10 = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000};
    private static int powerOfTen(int pow) {
        return POWERS_OF_10[pow];
    }

    @Override
    public void generate(List<Instruction> instructions) {
        //nacteme cele a desetinne cislo, napr 12.5 se nacte jako 12 a 5
        int firstI = Integer.parseInt(first);
        int secondI = Integer.parseInt(second);

        //jmenovatel bude 10 ^ delka jmenovatele
        //v pripade jmenovatele 5 bude jmenovatel 10
        int denominator = powerOfTen(second.length());

        //citatel bude hodnota celeho cisla * jmenovatel + hodnota desetinneho cisla
        //v pripade 12 a 5 dostavame 125/10
        int numerator = firstI * denominator + secondI;

        //zkratime v prekladaci, abychom nemuseli volat instrukce pl0 pro redukci
        Wrapper<Integer> wNumerator = new Wrapper<>(numerator);
        Wrapper<Integer> wDenominator = new Wrapper<>(denominator);
        reduce(wNumerator, wDenominator);

        //nacteni citatele i jmenovatele do zasobniku
        instructions.add(new PL0Instruction(InstructionType.LIT, wNumerator.getRef()));
        instructions.add(new PL0Instruction(InstructionType.LIT, wDenominator.getRef()));
    }


    //v ramci optimalizace provedeme kraceni ve zlomku uz v prekladaci
    private void reduce(Wrapper<Integer> numerator, Wrapper<Integer> denominator) {
        int divisor = gcd(numerator.getRef(), denominator.getRef());
        numerator.setRef(numerator.getRef() / divisor);
        denominator.setRef(denominator.getRef() / divisor);
    }

    //nejvetsi spolecny delitel
    private int gcd(int a, int b) {
        int c = a % b;
        if(c == 0) {
            return b;
        } else {
            return gcd(b, c);
        }
    }


    @Override
    public Type getType() {
        return Type.RATIO;
    }
}
