package fjp.sp.simplegrammar.actions.values;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.conversions.*;
import fjp.sp.simplegrammar.instructions.*;
import fjp.sp.simplegrammar.symbols.Context;
import fjp.sp.simplegrammar.symbols.VarDescriptor;
import fjp.sp.simplegrammar.utils.Wrapper;

import java.util.List;

public class VarAssignment extends Action {

    //akce AST pro ulozeni promenne ze zasobniku na adresu

    //nazev promenne
    private String variableName;

    //expression, ktery budeme vyhodnocovat a ukldata
    private Action expression;

    public VarAssignment(String variableName, Action expression, Context context) {
        this.variableName = variableName;
        this.expression = expression;
        this.context = context;
    }

    @Override
    public void generate(List<Instruction> instructions) {
        //vygenerujeme instrukce
        expression.generate(instructions);

        //nacteme symbol z tabulky symbolu
        Wrapper<Integer> level = new Wrapper<>(0);
        VarDescriptor symbol = this.context.assign(variableName, level);


        VarConversion conversion = new VarConversion(symbol.getType(), expression.getType());

        //provedeme konverzi, neplatna zahlasi chybu a ukonci preklad
        conversion.generate(instructions);

        //podle delky typu vygenerujeme instrukce STO
        for(int i = symbol.getType().getLength() - 1; i >= 0; i--) {
            PL0Instruction instruction = new PL0Instruction(
                    InstructionType.STO,
                    level.getRef(),
                    symbol.getAddress() + i);
            instructions.add(instruction);
        }


    }

    @Override
    public Type getType() {
        return this.context.getType(variableName);
    }
}
