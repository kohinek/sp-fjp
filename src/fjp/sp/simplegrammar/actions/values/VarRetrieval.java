package fjp.sp.simplegrammar.actions.values;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.instructions.*;
import fjp.sp.simplegrammar.symbols.Context;
import fjp.sp.simplegrammar.symbols.VarDescriptor;
import fjp.sp.simplegrammar.utils.Wrapper;

import java.util.List;

public class VarRetrieval extends Action {

    //ziskani promenne z adresy na zasobnik

    //jmeno promenne
    private String identifier;

    public VarRetrieval(String identifier, Context context) {
        this.identifier = identifier;
        this.context = context;
    }

    @Override
    public void generate(List<Instruction> instructions) {
        //podivame se do tabulky symbolu a vygenerujeme instrukce LOD
        Wrapper<Integer> level = new Wrapper<>(0);
        VarDescriptor symbol = this.context.retrieve(identifier, level);
        generate(level.getRef(), symbol.getAddress(), symbol.getType().getLength(), instructions);

    }

    private void generate(int level, int address, int length, List<Instruction> instructions) {
        for(int i = 0; i < length; i++) {
            instructions.add(new PL0Instruction(InstructionType.LOD, level, address + i));
        }
    }



    @Override
    public Type getType() {
        return context.getType(identifier);
    }
}
