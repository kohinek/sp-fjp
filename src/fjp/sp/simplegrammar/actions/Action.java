package fjp.sp.simplegrammar.actions;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.instructions.Instruction;
import fjp.sp.simplegrammar.symbols.Context;

import java.util.List;

public abstract class Action {

    //abstraktni trida akce v AST

    //odkaz na tabulku symbolu
    protected Context context;

    //vygenerovani instrukci
    abstract public void generate(List<Instruction> instructions);

    //ziskani typu akce
    public abstract Type getType();

    //ziskani delky akce
    public int getLength() {
        return getType().getLength();
    }

    //jestli akce jako posledni akci obsahuje return
    //defaultne je false
    //block statement akce prekryje tak, ze se podiva na posledni akci v bloku a zkontroluje, ze to je return
    public boolean hasReturn() {
        return false;
    }
}
