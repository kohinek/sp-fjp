package fjp.sp.simplegrammar.actions.expressions.operations;

import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.instructions.Instruction;
import fjp.sp.simplegrammar.instructions.InstructionType;
import fjp.sp.simplegrammar.instructions.Operation;
import fjp.sp.simplegrammar.instructions.PL0Instruction;

import java.util.List;

public class OrOperator implements Operator {

    //operace OR
    //pro booleany provede operaci pro ostatni zahlasi chybu
    //or probiha tak, ze booleany secte a podiva se, jestli je vysledek vetsi roven 1

    //0 + 0 = 0
    //1 + 0 = 1
    //1 + 1 = 1

    @Override
    public void generateBoolCalculation(List<Instruction> instructions) {
        instructions.add(new PL0Instruction(InstructionType.OPR, Operation.PLUS));
        instructions.add(new PL0Instruction(InstructionType.LIT, 1));
        instructions.add(new PL0Instruction(InstructionType.OPR, Operation.GE));
    }

    @Override
    public void generateRatioCalculation(List<Instruction> instructions) {
        ErrorHandler.getInstance().error("Cannot perform operation OR with ratio values.");
    }

    @Override
    public void generateIntCalculation(List<Instruction> instructions) {
        ErrorHandler.getInstance().error("Cannot perform operation OR with int values.");
    }

    @Override
    public String getName() {
        return "OR";
    }
}
