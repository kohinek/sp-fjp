package fjp.sp.simplegrammar.actions.expressions.operations;

import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.instructions.Instruction;

import java.util.List;

public class UPlusOperator implements Operator {

    //unarni plus
    //pro boolean zahlasi chybu
    //pro ratio a int neprovede nic

    @Override
    public void generateBoolCalculation(List<Instruction> instructions) {
        ErrorHandler.getInstance().error("Cannot perform operation UPLUS with boolean values.");
    }

    @Override
    public void generateRatioCalculation(List<Instruction> instructions) {
        //EMPTY
    }

    @Override
    public void generateIntCalculation(List<Instruction> instructions) {
        //EMPTY
    }

    @Override
    public String getName() {
        return "UPLUS";
    }
}
