package fjp.sp.simplegrammar.actions.expressions.operations;

import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.instructions.Instruction;
import fjp.sp.simplegrammar.instructions.InstructionType;
import fjp.sp.simplegrammar.instructions.Operation;
import fjp.sp.simplegrammar.instructions.PL0Instruction;

import java.util.List;

public class NegOperator implements Operator {

    //operace negace
    //u booleanu provede negaci
    //negaci provadime vlastne jako 1 - hodnota
    //v praxi pak (hodnota - 1) * (-1)

    @Override
    public void generateBoolCalculation(List<Instruction> instructions) {
        instructions.add(new PL0Instruction(InstructionType.LIT, 1));
        instructions.add(new PL0Instruction(InstructionType.OPR, Operation.MINUS));
        instructions.add(new PL0Instruction(InstructionType.LIT, -1));
        instructions.add(new PL0Instruction(InstructionType.OPR, Operation.MUL));

    }

    @Override
    public void generateRatioCalculation(List<Instruction> instructions) {
        ErrorHandler.getInstance().error("Cannot perform operation NEGATION with ratio value.");
    }

    @Override
    public void generateIntCalculation(List<Instruction> instructions) {
        ErrorHandler.getInstance().error("Cannot perform operation NEGATION with int value.");
    }

    @Override
    public String getName() {
        return "NEGATION";
    }
}
