package fjp.sp.simplegrammar.actions.expressions.operations;

import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.instructions.*;

import java.util.List;

public class StandardOperator implements Operator {

    //operator pro standardni operace, pro ktere existuje instrukc PL0

    private Operation operation;

    public StandardOperator(Operation operation) {
        this.operation = operation;
    }


    //standardni operace, ktere nepodporuji boolean by mely byt odchyceny v tabulkach v binary a unary expression
    @Override
    public void generateBoolCalculation(List<Instruction> instructions) {
        instructions.add(new PL0Instruction(InstructionType.OPR, operation));
    }

    //ratio operaci provedeme tak, ze zavolame knihovnu ratio se jmenem operace
    @Override
    public void generateRatioCalculation(List<Instruction> instructions) {
        String name = "ratio" + operation.name().toLowerCase();
        instructions.add(new LibraryCall(name));
    }

    //INT provedeme tak, ze pridame prislusnou instrukci
    @Override
    public void generateIntCalculation(List<Instruction> instructions) {
        instructions.add(new PL0Instruction(InstructionType.OPR, operation));
    }

    @Override
    public String getName() {
        return operation.name();
    }
}
