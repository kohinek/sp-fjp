package fjp.sp.simplegrammar.actions.expressions.operations;

import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.instructions.Instruction;
import fjp.sp.simplegrammar.instructions.InstructionType;
import fjp.sp.simplegrammar.instructions.Operation;
import fjp.sp.simplegrammar.instructions.PL0Instruction;

import java.util.List;

public class AndOperator implements Operator {

    //OPERATOR AND
    //booleany vynasobi
    //1 * 1 = 0
    //0 * 0 = 0
    //1 * 0 = 0
    //presne, co chceme

    //u ostatnich zahlasi chybu

    @Override
    public void generateBoolCalculation(List<Instruction> instructions) {
        instructions.add(new PL0Instruction(InstructionType.OPR, Operation.MUL));
    }

    @Override
    public void generateRatioCalculation(List<Instruction> instructions) {
        ErrorHandler.getInstance().error("Cannot perform operation AND with ratio values.");
    }

    @Override
    public void generateIntCalculation(List<Instruction> instructions) {
        ErrorHandler.getInstance().error("Cannot perform operation AND with int values.");
    }

    @Override
    public String getName() {
        return "AND";
    }
}
