package fjp.sp.simplegrammar.actions.expressions.operations;

import fjp.sp.simplegrammar.instructions.Instruction;

import java.util.List;

public interface Operator {

    //operace (mohou byt +, -, AND, negace atd.)
    //vygeneruji konkretni kod operace, pokud uz mam v zasobniku nactene hodnoty

    void generateBoolCalculation(List<Instruction> instructions);
    void generateRatioCalculation(List<Instruction> instructions);
    void generateIntCalculation(List<Instruction> instructions);

    String getName();
}
