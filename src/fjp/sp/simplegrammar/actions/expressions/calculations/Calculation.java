package fjp.sp.simplegrammar.actions.expressions.calculations;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;
import fjp.sp.simplegrammar.instructions.Instruction;

import java.util.List;

public interface Calculation {

    //kalkulace
    //diky ni poznam, kterou metodu operatoru mam zavolat

    //boolean kalkulace zavola boolean metodu operatoru
    //int kalkualce zavola int metodu operatoru
    //ratio kalkulace zavola ratio metodu operatoru
    void generate(List<Instruction> instructions, Operator operator);
}
