package fjp.sp.simplegrammar.actions.expressions.calculations;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;
import fjp.sp.simplegrammar.instructions.Instruction;

import java.util.List;

public class IntCalculation implements Calculation {

    //viz interface
    @Override
    public void generate(List<Instruction> instructions, Operator operator) {
        operator.generateIntCalculation(instructions);
    }

}
