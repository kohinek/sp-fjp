package fjp.sp.simplegrammar.actions.expressions.conversions;

import fjp.sp.simplegrammar.instructions.Instruction;

import java.util.List;

public class EmptyConversion implements Conversion{

    //prazdna konverzce, pouzivame, kdyz prevadime typ na stejny typ

    @Override
    public void generate(List<Instruction> instructions) {

    }
}
