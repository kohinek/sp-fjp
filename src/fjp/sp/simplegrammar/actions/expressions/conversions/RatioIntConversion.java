package fjp.sp.simplegrammar.actions.expressions.conversions;

import fjp.sp.simplegrammar.instructions.Instruction;
import fjp.sp.simplegrammar.instructions.InstructionType;
import fjp.sp.simplegrammar.instructions.Operation;
import fjp.sp.simplegrammar.instructions.PL0Instruction;

import java.util.List;

public class RatioIntConversion implements Conversion{

    //ratio -> int
    //provedeme celociselne vydeleni citatele a jmenovatele

    @Override
    public void generate(List<Instruction> instructions) {
        instructions.add(new PL0Instruction(InstructionType.OPR, Operation.DIV));
    }
}
