package fjp.sp.simplegrammar.actions.expressions.conversions;

import fjp.sp.simplegrammar.instructions.Instruction;
import fjp.sp.simplegrammar.instructions.InstructionType;
import fjp.sp.simplegrammar.instructions.PL0Instruction;

import java.util.List;

public class IntRatioConversion implements Conversion {

    //konverze INT -> RATIO
    //provedeme pridani jmenovatele 1


    @Override
    public void generate(List<Instruction> instructions) {
        instructions.add(new PL0Instruction(InstructionType.LIT, 1));
    }
}
