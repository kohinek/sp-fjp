package fjp.sp.simplegrammar.actions.expressions.conversions;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.instructions.Instruction;

import java.util.List;

public class InvalidConversion implements Conversion {

    //nevalidni konverze
    //napriklad, kdyz chceme dat ratio do booleanu
    //zahlasime chybu

    private Type from;
    private Type to;

    public InvalidConversion(Type from, Type to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public void generate(List<Instruction> instructions) {
        ErrorHandler.getInstance().error("Can't convert type " + from.name() + " to type " + to.name());
    }
}
