package fjp.sp.simplegrammar.actions.expressions.conversions;

import fjp.sp.simplegrammar.instructions.Instruction;

import java.util.List;

public interface Conversion {

    //prevedeni jednoho typu na druhy
    void generate(List<Instruction> instructions);
}
