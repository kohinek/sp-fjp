package fjp.sp.simplegrammar.actions.expressions.strategy;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;
import fjp.sp.simplegrammar.instructions.Instruction;

import java.util.List;

public interface Strategy {

    //kazda strategie konkretne definuje, jak vygenerujeme nejaky expression
    //vraci ji strategy factory
    void generate(List<Instruction> instructions);
    Type getReturnType();
}
