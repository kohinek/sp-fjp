package fjp.sp.simplegrammar.actions.expressions.strategy;

import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;

public interface StrategyFactory {

    //pro kazdy expression je staticky vytvorena tabulka se strategy factory
    //pri zalozeni konkretni expressiony je zavolana funkce get instance, ktera vrati konkretni strategii
    //v podstate se jedna o neco na zpusob abstraktni tovarny
    Strategy getInstance(Action expression1, Action expression2, Operator operator);
}
