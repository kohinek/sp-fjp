package fjp.sp.simplegrammar.actions.expressions.strategy;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.calculations.Calculation;
import fjp.sp.simplegrammar.actions.expressions.conversions.Conversion;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;
import fjp.sp.simplegrammar.instructions.Instruction;

import java.util.List;

public class UnaryStrategy implements Strategy {

    //unarni strategie

    //staticke parametery, prevzaty z tovarny
    private Conversion conversion;
    private Calculation calculation;
    private Type type;

    //dynamicke parametry, vytvoreny v metode getInstace, konkretni hodnoty strategie
    private Action expression;
    private Operator operator;

    UnaryStrategy(Conversion conversion, Calculation calculation, Type type, Action expression1, Operator operator) {
        this.conversion = conversion;
        this.calculation = calculation;
        this.type = type;
        this.expression = expression1;
        this.operator = operator;
    }

    @Override
    public void generate(List<Instruction> instructions) {
        //unarni strategie vygeneruje expression, prevede ji a vygeneru kod operace
        expression.generate(instructions);
        conversion.generate(instructions);
        calculation.generate(instructions, operator);
    }

    @Override
    public Type getReturnType() {
        return this.type;
    }

}
