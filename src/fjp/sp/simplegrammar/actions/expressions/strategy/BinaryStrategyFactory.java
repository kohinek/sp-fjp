package fjp.sp.simplegrammar.actions.expressions.strategy;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.calculations.Calculation;
import fjp.sp.simplegrammar.actions.expressions.conversions.Conversion;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;

public class BinaryStrategyFactory implements StrategyFactory {

    //tovarna na binarni strategie

    private Conversion conversion1;
    private Conversion conversion2;
    private Calculation calculation;
    private Type type;

    //staticke promenne strategie, zname pri prekladu
    public BinaryStrategyFactory(Conversion conversion1, Conversion conversion2, Calculation calculation, Type type) {
        this.conversion1 = conversion1;
        this.conversion2 = conversion2;
        this.calculation = calculation;
        this.type = type;
    }

    //dynamicke promenne strategie, zname pri zalozeni expressionu v AST
    @Override
    public Strategy getInstance(Action expression1, Action expression2, Operator operator) {
        return new BinaryStrategy(conversion1, conversion2, calculation, type, expression1, expression2, operator);
    }
}
