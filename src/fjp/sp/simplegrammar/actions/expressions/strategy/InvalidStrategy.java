package fjp.sp.simplegrammar.actions.expressions.strategy;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;
import fjp.sp.simplegrammar.error.ErrorHandler;
import fjp.sp.simplegrammar.instructions.Instruction;

import java.util.List;

public class InvalidStrategy implements Strategy {

    //nevalidni strategie (napr kdyz chceme udelat boolean + boolean)

    private Action expression1;
    private Action expression2;
    private Operator operator;

    InvalidStrategy(Action expression1, Action expression2, Operator operator) {
        this.expression1 = expression1;
        this.expression2 = expression2;
        this.operator = operator;
    }

    @Override
    public void generate(List<Instruction> instructions) {
        //vygenerujeme chybovou zpravu
        String second = expression2 == null ? "" : " and " + expression2.getType().name();
        String message = "Can't perform operation " + operator.getName() + " with " + expression1.getType() + second + ".";
        ErrorHandler.getInstance().error(message);
    }

    @Override
    public Type getReturnType() {
        return Type.VOID;
    }
}
