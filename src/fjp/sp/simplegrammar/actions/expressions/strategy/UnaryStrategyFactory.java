package fjp.sp.simplegrammar.actions.expressions.strategy;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.calculations.Calculation;
import fjp.sp.simplegrammar.actions.expressions.conversions.Conversion;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;

public class UnaryStrategyFactory implements StrategyFactory {

    //tovarna pro unarni strategie

    private Conversion conversion;
    private Calculation calculation;
    private Type type;

    public UnaryStrategyFactory(Conversion conversion, Calculation calculation, Type type) {
        this.conversion = conversion;
        this.calculation = calculation;
        this.type = type;
    }

    @Override
    public Strategy getInstance(Action expression1, Action expression2, Operator operator) {
        return new UnaryStrategy(conversion, calculation, type, expression1, operator);
    }
}
