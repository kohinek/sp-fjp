package fjp.sp.simplegrammar.actions.expressions.strategy;

import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;

public class InvalidStrategyFactory implements StrategyFactory {

    //tovarna nevalidnich strategii
    //pouze vrati novou nevalidni strategii

    @Override
    public Strategy getInstance(Action expression1, Action expression2, Operator operator) {
        return new InvalidStrategy(expression1, expression2, operator);
    }
}
