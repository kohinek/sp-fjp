package fjp.sp.simplegrammar.actions.expressions.strategy;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.calculations.Calculation;
import fjp.sp.simplegrammar.actions.expressions.conversions.Conversion;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;
import fjp.sp.simplegrammar.instructions.Instruction;

import java.util.List;

public class BinaryStrategy implements Strategy{

    //strategie pro binarni expressiony

    //staticke parametery, prevzaty z tovarny
    private Conversion conversion1;
    private Conversion conversion2;
    private Calculation calculation;
    private Type type;

    //dynamicke parametry, vytvoreny v metode getInstance, konkretni hodnoty strategie
    private Action expression1;
    private Action expression2;
    private Operator operator;

    BinaryStrategy(Conversion conversion1, Conversion conversion2, Calculation calculation, Type type, Action expression1, Action expression2, Operator operator) {
        this.conversion1 = conversion1;
        this.conversion2 = conversion2;
        this.calculation = calculation;
        this.type = type;
        this.expression1 = expression1;
        this.expression2 = expression2;
        this.operator = operator;
    }

    @Override
    public Type getReturnType() {
        return this.type;
    }

    @Override
    public void generate(List<Instruction> instructions){
        //strategie probiha tak, ze vygenerujeme expression
        expression1.generate(instructions);
        //prevedeme ho
        conversion1.generate(instructions);
        //vygenerujeme druhy expression
        expression2.generate(instructions);
        //prevedeme ho
        conversion2.generate(instructions);
        //a provedeme nasi operaci
        calculation.generate(instructions, operator);
    }
}
