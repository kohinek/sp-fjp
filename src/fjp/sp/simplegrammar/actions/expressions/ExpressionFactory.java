package fjp.sp.simplegrammar.actions.expressions;

import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.instances.*;
import fjp.sp.simplegrammar.actions.expressions.operations.*;
import fjp.sp.simplegrammar.instructions.Operation;

//factory trida pro expressiony
//vrati dany expression podle textu operatoru

public class ExpressionFactory {
    //binarni expression
    //vola expressiony ktere dedi od binary expression
    //textovy oeprator prevede na tridu operator a jako parametry prida expression
    public static BinaryExpression getExpressionByTitle(String title, Action exprLeft, Action exprRight) {
        switch(title) {
            case "&&":
                return new LogicalExpression(exprLeft, exprRight, new AndOperator());
            case "||":
                return new LogicalExpression(exprLeft, exprRight, new OrOperator());
            case "==":
                return new EqualExpression(exprLeft, exprRight, new StandardOperator(Operation.EQ));
            case "!=":
                return new EqualExpression(exprLeft, exprRight, new StandardOperator(Operation.NOT_EQ));
            case ">":
                return new ComparsionExpression(exprLeft, exprRight, new StandardOperator(Operation.G));
            case ">=":
                return new ComparsionExpression(exprLeft, exprRight, new StandardOperator(Operation.GE));
            case "<":
                return new ComparsionExpression(exprLeft, exprRight, new StandardOperator(Operation.L));
            case "<=":
                return new ComparsionExpression(exprLeft, exprRight, new StandardOperator(Operation.LE));
            case "+":
                return new NumericalExpression(exprLeft, exprRight, new StandardOperator(Operation.PLUS));
            case "-":
                return new NumericalExpression(exprLeft, exprRight, new StandardOperator(Operation.MINUS));
            case "*":
                return new NumericalExpression(exprLeft, exprRight, new StandardOperator(Operation.MUL));
            case "/":
                return new DivExpression(exprLeft, exprRight, new StandardOperator(Operation.DIV));
            case "%":
                return new ModExpression(exprLeft, exprRight, new StandardOperator(Operation.MOD));
        }

        return null;
    }

    //volani unarnich expressionu
    public static UnaryExpression getExpressionByTitle(String title, Action exprRight) {
        switch(title) {
            case "!":
                return new NegationExpression(exprRight, new NegOperator());
            case "+":
                return new UnaryNumericalExpression(exprRight, new UPlusOperator());
            case "-":
                return new UnaryNumericalExpression(exprRight, new StandardOperator(Operation.UMINUS));
        }

        return null;
    }


}
