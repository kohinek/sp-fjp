package fjp.sp.simplegrammar.actions.expressions.instances;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;
import fjp.sp.simplegrammar.actions.expressions.strategy.InvalidStrategy;
import fjp.sp.simplegrammar.actions.expressions.strategy.InvalidStrategyFactory;
import fjp.sp.simplegrammar.actions.expressions.strategy.Strategy;
import fjp.sp.simplegrammar.actions.expressions.strategy.StrategyFactory;

public class UnaryExpression extends Expression {

    //unarni vyrazy

    //jejich tabulka je pouze jednorozmerna

    static StrategyFactory[] getDefaultTable() {
        StrategyFactory[] table;
        int count = Type.getCount();
        table = new StrategyFactory[count];

        for(int i = 0; i < count; i++) {
            table[i] = new InvalidStrategyFactory();
        }

        return table;

    }

    UnaryExpression(Action expression, Operator operator, StrategyFactory[] table) {
        StrategyFactory factory = table[expression.getType().getIndex()];
        this.strategy = factory.getInstance(expression, null, operator);
    }
}
