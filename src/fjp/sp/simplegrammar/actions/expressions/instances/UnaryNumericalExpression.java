package fjp.sp.simplegrammar.actions.expressions.instances;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.calculations.IntCalculation;
import fjp.sp.simplegrammar.actions.expressions.calculations.RatioCalculation;
import fjp.sp.simplegrammar.actions.expressions.conversions.EmptyConversion;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;
import fjp.sp.simplegrammar.actions.expressions.strategy.StrategyFactory;
import fjp.sp.simplegrammar.actions.expressions.strategy.UnaryStrategyFactory;

public class UnaryNumericalExpression extends UnaryExpression {

    //vyhodnoceni unarnich aritmetickych vyrazu + a -

    private static StrategyFactory[] table;

    static{
        table = getDefaultTable();
        EmptyConversion emptyConversion = new EmptyConversion();
        IntCalculation intCalculation = new IntCalculation();
        RatioCalculation ratioCalculation = new RatioCalculation();

        table[Type.INTEGER.getIndex()] = new UnaryStrategyFactory(emptyConversion, intCalculation, Type.INTEGER);
        table[Type.RATIO.getIndex()] = new UnaryStrategyFactory(emptyConversion, ratioCalculation, Type.RATIO);
    }

    public UnaryNumericalExpression(Action expression, Operator operator) {
        super(expression, operator, table);
    }
}
