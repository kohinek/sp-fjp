package fjp.sp.simplegrammar.actions.expressions.instances;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.calculations.RatioCalculation;
import fjp.sp.simplegrammar.actions.expressions.conversions.EmptyConversion;
import fjp.sp.simplegrammar.actions.expressions.conversions.IntRatioConversion;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;
import fjp.sp.simplegrammar.actions.expressions.strategy.BinaryStrategyFactory;
import fjp.sp.simplegrammar.actions.expressions.strategy.StrategyFactory;

public class DivExpression extends BinaryExpression {

    //expression pro deleni

    private static StrategyFactory[][] table;


    static{
        table = getDefaultTable();
        //deleni je pouze typu ratio
        //pokud delime inty, prevedeme je na ratio a pak provedeme deleni


        EmptyConversion emptyConversion = new EmptyConversion();
        IntRatioConversion intRatioConversion = new IntRatioConversion();
        RatioCalculation ratioCalculation = new RatioCalculation();

        table[Type.INTEGER.getIndex()][Type.INTEGER.getIndex()] = new BinaryStrategyFactory(intRatioConversion, intRatioConversion, ratioCalculation, Type.RATIO);
        table[Type.RATIO.getIndex()][Type.INTEGER.getIndex()] = new BinaryStrategyFactory(emptyConversion, intRatioConversion, ratioCalculation, Type.RATIO);
        table[Type.INTEGER.getIndex()][Type.RATIO.getIndex()] = new BinaryStrategyFactory(intRatioConversion, emptyConversion, ratioCalculation, Type.RATIO);
        table[Type.RATIO.getIndex()][Type.RATIO.getIndex()] = new BinaryStrategyFactory(emptyConversion, emptyConversion, ratioCalculation, Type.RATIO);
    }

    public DivExpression(Action expression1, Action expression2, Operator op) {
        super(expression1, expression2, op, table);
    }
}
