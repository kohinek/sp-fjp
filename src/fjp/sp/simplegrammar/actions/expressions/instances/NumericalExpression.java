package fjp.sp.simplegrammar.actions.expressions.instances;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.calculations.IntCalculation;
import fjp.sp.simplegrammar.actions.expressions.calculations.RatioCalculation;
import fjp.sp.simplegrammar.actions.expressions.conversions.EmptyConversion;
import fjp.sp.simplegrammar.actions.expressions.conversions.IntRatioConversion;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;
import fjp.sp.simplegrammar.actions.expressions.strategy.BinaryStrategy;
import fjp.sp.simplegrammar.actions.expressions.strategy.BinaryStrategyFactory;
import fjp.sp.simplegrammar.actions.expressions.strategy.Strategy;
import fjp.sp.simplegrammar.actions.expressions.strategy.StrategyFactory;

public class NumericalExpression extends BinaryExpression {

    //binarni expression pro standardni numericke operatory (+, -, *)
    //deleni probiha pouze ratio, takze ma vlastni tridu
    //modulo naopak muze byt pouze int

    private static StrategyFactory[][] table;

    static {
        table = getDefaultTable();
        //conversion
        EmptyConversion emptyConversion = new EmptyConversion();
        IntRatioConversion intRatioConversion = new IntRatioConversion();

        //calculations
        IntCalculation intCalculation = new IntCalculation();
        RatioCalculation ratioCalculation = new RatioCalculation();



        //INT + INT -> INT
        table[Type.INTEGER.getIndex()][Type.INTEGER.getIndex()] = new BinaryStrategyFactory(emptyConversion, emptyConversion, intCalculation, Type.INTEGER);

        //RATIO + INT -> RATIO
        table[Type.INTEGER.getIndex()][Type.RATIO.getIndex()] = new BinaryStrategyFactory(intRatioConversion, emptyConversion, ratioCalculation, Type.RATIO);
        table[Type.RATIO.getIndex()][Type.INTEGER.getIndex()] = new BinaryStrategyFactory(emptyConversion, intRatioConversion, ratioCalculation, Type.RATIO);

        //RATIO + RATIO -> RATIO
        table[Type.RATIO.getIndex()][Type.RATIO.getIndex()] = new BinaryStrategyFactory(emptyConversion, emptyConversion, ratioCalculation, Type.RATIO);
    }

    public NumericalExpression(Action expression1, Action expression2, Operator op) {
        super(expression1, expression2, op, table);
    }
}
