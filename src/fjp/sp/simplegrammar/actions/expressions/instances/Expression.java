package fjp.sp.simplegrammar.actions.expressions.instances;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.strategy.Strategy;
import fjp.sp.simplegrammar.instructions.Instruction;

import java.util.List;

public class Expression extends Action {

    //hlavni expression
    //kazda expression vyuziva strategii, podle ktere postupuje
    //expression pouze deleguje vykonani na strategii

    protected Strategy strategy;

    @Override
    public void generate(List<Instruction> instructions) {
        this.strategy.generate(instructions);
    }

    @Override
    public Type getType() {
        return this.strategy.getReturnType();
    }



}
