package fjp.sp.simplegrammar.actions.expressions.instances;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.calculations.IntCalculation;
import fjp.sp.simplegrammar.actions.expressions.conversions.EmptyConversion;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;
import fjp.sp.simplegrammar.actions.expressions.strategy.BinaryStrategyFactory;
import fjp.sp.simplegrammar.actions.expressions.strategy.StrategyFactory;

public class ModExpression extends BinaryExpression {

    //pro modulo

    private static StrategyFactory[][] table;

    static{

        table = getDefaultTable();
        EmptyConversion emptyConversion = new EmptyConversion();
        IntCalculation intCalculation = new IntCalculation();

        //INT % INT -> INT
        //jinak chyba

        table[Type.INTEGER.getIndex()][Type.INTEGER.getIndex()] = new BinaryStrategyFactory(emptyConversion, emptyConversion, intCalculation, Type.INTEGER);
    }

    public ModExpression(Action expression1, Action expression2, Operator op) {
        super(expression1, expression2, op, table);
    }
}
