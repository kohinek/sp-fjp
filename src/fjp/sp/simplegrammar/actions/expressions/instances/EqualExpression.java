package fjp.sp.simplegrammar.actions.expressions.instances;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.calculations.BoolCalculation;
import fjp.sp.simplegrammar.actions.expressions.calculations.IntCalculation;
import fjp.sp.simplegrammar.actions.expressions.calculations.RatioCalculation;
import fjp.sp.simplegrammar.actions.expressions.conversions.EmptyConversion;
import fjp.sp.simplegrammar.actions.expressions.conversions.IntRatioConversion;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;
import fjp.sp.simplegrammar.actions.expressions.strategy.BinaryStrategyFactory;
import fjp.sp.simplegrammar.actions.expressions.strategy.Strategy;
import fjp.sp.simplegrammar.actions.expressions.strategy.StrategyFactory;

public class EqualExpression extends BinaryExpression {

    //expression pro == a !=
    //rozdil oproti >, < atd. je v tom, ze = a != lze pouzit i pro booleany

    private static StrategyFactory[][] table;

    static {
        table = getDefaultTable();

        //conversion
        EmptyConversion emptyConversion = new EmptyConversion();
        IntRatioConversion intRatioConversion = new IntRatioConversion();

        //calculations
        IntCalculation intCalculation = new IntCalculation();
        RatioCalculation ratioCalculation = new RatioCalculation();
        BoolCalculation boolCalculation = new BoolCalculation();



        //INT + INT -> INT
        table[Type.INTEGER.getIndex()][Type.INTEGER.getIndex()] = new BinaryStrategyFactory(emptyConversion, emptyConversion, intCalculation, Type.BOOLEAN);

        //RATIO + INT -> RATIO
        table[Type.INTEGER.getIndex()][Type.RATIO.getIndex()] = new BinaryStrategyFactory(intRatioConversion, emptyConversion, ratioCalculation, Type.BOOLEAN);
        table[Type.RATIO.getIndex()][Type.INTEGER.getIndex()] = new BinaryStrategyFactory(emptyConversion, intRatioConversion, ratioCalculation, Type.BOOLEAN);

        //RATIO + RATIO -> RATIO
        table[Type.RATIO.getIndex()][Type.RATIO.getIndex()] = new BinaryStrategyFactory(emptyConversion, emptyConversion, ratioCalculation, Type.BOOLEAN);

        //BOOL + BOOL -> BOOL
        table[Type.BOOLEAN.getIndex()][Type.BOOLEAN.getIndex()] = new BinaryStrategyFactory(emptyConversion, emptyConversion, boolCalculation, Type.BOOLEAN);
    }

    public EqualExpression(Action expression1, Action expression2, Operator op) {
        super(expression1, expression2, op, table);
    }
}