package fjp.sp.simplegrammar.actions.expressions.instances;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.calculations.IntCalculation;
import fjp.sp.simplegrammar.actions.expressions.calculations.RatioCalculation;
import fjp.sp.simplegrammar.actions.expressions.conversions.EmptyConversion;
import fjp.sp.simplegrammar.actions.expressions.conversions.IntRatioConversion;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;
import fjp.sp.simplegrammar.actions.expressions.strategy.BinaryStrategyFactory;
import fjp.sp.simplegrammar.actions.expressions.strategy.StrategyFactory;

public class ComparsionExpression extends BinaryExpression {

    //pro operace  >=, >, <, <=
    //od numerical expression se lisi typem navratove hodnoty
    //od equal expression se lisi tim, ze equal muze porovnat bool hodnoty

    private static StrategyFactory[][] table;

    static {
        table = getDefaultTable();
        //conversion
        EmptyConversion emptyConversion = new EmptyConversion();
        IntRatioConversion intRatioConversion = new IntRatioConversion();

        //calculations
        IntCalculation intCalculation = new IntCalculation();
        RatioCalculation ratioCalculation = new RatioCalculation();



        //INT >= INT -> BOOLEAN
        table[Type.INTEGER.getIndex()][Type.INTEGER.getIndex()] = new BinaryStrategyFactory(emptyConversion, emptyConversion, intCalculation, Type.BOOLEAN);

        //RATIO >= INT -> BOOLEAN
        table[Type.INTEGER.getIndex()][Type.RATIO.getIndex()] = new BinaryStrategyFactory(intRatioConversion, emptyConversion, ratioCalculation, Type.BOOLEAN);
        table[Type.RATIO.getIndex()][Type.INTEGER.getIndex()] = new BinaryStrategyFactory(emptyConversion, intRatioConversion, ratioCalculation, Type.BOOLEAN);

        //RATIO >= RATIO -> BOOLEAN
        table[Type.RATIO.getIndex()][Type.RATIO.getIndex()] = new BinaryStrategyFactory(emptyConversion, emptyConversion, ratioCalculation, Type.BOOLEAN);
    }


    public ComparsionExpression(Action expression1, Action expression2, Operator op) {
        super(expression1, expression2, op, table);
    }
}
