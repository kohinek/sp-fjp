package fjp.sp.simplegrammar.actions.expressions.instances;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.calculations.BoolCalculation;
import fjp.sp.simplegrammar.actions.expressions.conversions.EmptyConversion;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;
import fjp.sp.simplegrammar.actions.expressions.strategy.BinaryStrategyFactory;
import fjp.sp.simplegrammar.actions.expressions.strategy.StrategyFactory;

public class LogicalExpression extends BinaryExpression {

    //pro logicke operace

    private static StrategyFactory[][] table;

    static{
        table = getDefaultTable();

        EmptyConversion emptyConversion = new EmptyConversion();
        BoolCalculation boolCalculation = new BoolCalculation();

        table[Type.BOOLEAN.getIndex()][Type.BOOLEAN.getIndex()] = new BinaryStrategyFactory(emptyConversion, emptyConversion, boolCalculation, Type.BOOLEAN);
    }

    public LogicalExpression(Action expression1, Action expression2, Operator op) {
        super(expression1, expression2, op, table);
    }
}
