package fjp.sp.simplegrammar.actions.expressions.instances;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;
import fjp.sp.simplegrammar.actions.expressions.strategy.InvalidStrategy;
import fjp.sp.simplegrammar.actions.expressions.strategy.InvalidStrategyFactory;
import fjp.sp.simplegrammar.actions.expressions.strategy.Strategy;
import fjp.sp.simplegrammar.actions.expressions.strategy.StrategyFactory;

public class BinaryExpression extends Expression {

    //binarni expression

    //definuje staticky strategie, podle kterych budeme postupoat
    //do tabulky na pozice dame indexy obou expressions, ktere zpracovavame a vysledkem je stratgie, podle ktere budeme postupovat

    //kazda konkretni instance binary expression definuje svoji tabulku


    static StrategyFactory[][]  getDefaultTable() {
        //vytvoreni tabulky plne nevalidnich strategii

        StrategyFactory[][] table;
        int count = Type.getCount();
        table = new StrategyFactory[count][count];

        //defaultne nastavime chybu, konkretni instance prepisi chybovy stav
        for(int i = 0; i < count; i++) {
            for(int j = 0; j < count; j++) {
                table[i][j] = new InvalidStrategyFactory();
            }
        }

        return table;
    }


    BinaryExpression(Action expression1, Action expression2, Operator op, StrategyFactory[][] table){
        StrategyFactory factory = table[expression1.getType().getIndex()][expression2.getType().getIndex()];
        this.strategy = factory.getInstance(expression1, expression2, op);
    }
}
