package fjp.sp.simplegrammar.actions.expressions.instances;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.actions.expressions.calculations.BoolCalculation;
import fjp.sp.simplegrammar.actions.expressions.conversions.EmptyConversion;
import fjp.sp.simplegrammar.actions.expressions.operations.Operator;
import fjp.sp.simplegrammar.actions.expressions.strategy.StrategyFactory;
import fjp.sp.simplegrammar.actions.expressions.strategy.UnaryStrategyFactory;

public class NegationExpression extends UnaryExpression {

    //pro negaci

    private static StrategyFactory[] table;

    static{
        table = getDefaultTable();
        EmptyConversion emptyConversion = new EmptyConversion();
        BoolCalculation boolCalculation = new BoolCalculation();

        table[Type.BOOLEAN.getIndex()] = new UnaryStrategyFactory(emptyConversion, boolCalculation, Type.BOOLEAN);
    }

    public NegationExpression(Action expression, Operator operator) {
        super(expression, operator, table);
    }
}
