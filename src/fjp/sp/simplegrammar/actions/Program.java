package fjp.sp.simplegrammar.actions;

import fjp.sp.simplegrammar.Type;
import fjp.sp.simplegrammar.instructions.*;
import fjp.sp.simplegrammar.symbols.Context;
import fjp.sp.simplegrammar.symbols.MethodDescriptor;

import java.util.List;

public class Program extends Action {

    //Koren v AST
    //reprezentuje cely program

    //delka AZ
    public static final int ACTIVATION_RECORD_SIZE = 3;

    //jmeno metody main
    private static final String MAIN_METHOD_NAME = "main";

    //program obsahuje mnozinu deklaraci promennych a metod
    private List<Action> varDeclarations;
    private List<Action> methods;


    //konstruktor
    public Program(List<Action> varDeclarations, List<Action> methods, Context context) {
        this.varDeclarations = varDeclarations;
        this.methods = methods;
        this.context = context;
    }

    //vygenerovani instrukci
    @Override
    public void generate(List<Instruction> instructions) {
        //nacteme main
        MethodDescriptor descriptor = this.context.call(MAIN_METHOD_NAME);

        //inicializujeme AZ a ziskame misto pro globalni promenne
        instructions.add(new PL0Instruction(
                InstructionType.INT,
                0,
                new InstructionIntegerWrapper(ACTIVATION_RECORD_SIZE + context.getContextParamsLength())
        ));

        //nejdrive vygenerujeme deklarace globalnich promennych
        for(Action action : varDeclarations) {
            action.generate(instructions);
        }

        //pote zavolame funkci main
        instructions.add(new PL0Instruction(
                InstructionType.CAL,
                0,
                descriptor.getLabel()
        ));

        //ukonceni programu
        instructions.add(new PL0Instruction(
                InstructionType.RET,
                0,
                new InstructionIntegerWrapper(0)
        ));

        //vygenerujeme metody
        for(Action action : methods) {
            action.generate(instructions);
        }

    }

    @Override
    public Type getType() {
        return Type.VOID;
    }
}
