package fjp.sp.simplegrammar;



import fjp.sp.simplegrammar.actions.Action;
import fjp.sp.simplegrammar.grammar.GrammarLexer;
import fjp.sp.simplegrammar.grammar.GrammarParser;
import fjp.sp.simplegrammar.instructions.Instruction;
import fjp.sp.simplegrammar.instructions.InstructionSet;
import fjp.sp.simplegrammar.symbols.GlobalContext;
import fjp.sp.simplegrammar.visitor.Visitor;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class Main {


    public static void main(String[] args) {

        if(args.length != 2) {
            System.out.println("Usage: java -jar <program> <input> <output>");
            System.exit(1);
        }

        GrammarLexer lexer = null;
        try {
            lexer = new GrammarLexer(CharStreams.fromFileName(args[0]));
        } catch (IOException e) {
            System.out.println("Invalid input file");
            System.exit(1);
        }

        PrintWriter pw = null;
        try {
            pw = new PrintWriter(args[1]);
        } catch (FileNotFoundException e) {
            System.out.println("Invalid output file");
            System.exit(1);
        }


        //antlr zpracovani
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        GrammarParser parser = new GrammarParser(tokens);
        ParseTree tree = parser.program();
        Visitor globalContextVisitor = new Visitor(GlobalContext.getInstance());

        //pruchod visitory = derivacni strom -> AST
        Action program = globalContextVisitor.visit(tree);

        //pruchod AST a vytvoreni instrukci bez adresovani
        List<Instruction> instructions = new ArrayList<>();
        program.generate(instructions);

        //prelozeni labelu na adresy, zpracovani knihovnich funkci
        InstructionSet set = new InstructionSet(instructions);
        set.translate();

        //vypsani do vystupu
        pw.print(set.translationToString());
        pw.close();

        System.out.println("Compilation done.");

    }
}
