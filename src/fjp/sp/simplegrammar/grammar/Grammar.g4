grammar Grammar;

program: body;

body: ((varDeclarationLine ';') | method)* ;

statement
: '{' statement* '}'                                                                                  #blockStatement
| 'if' expression statement ('else' statement)?                                                       #ifStatement
| 'for' '(' (varDeclarationLine | varAssignment) ';' expression ';' varAssignment ')' statement       #forStatement
| 'while' '(' expression ')' statement                                                                #whileStatement
| varAssignment ';'                                                                                   #varAssignmentStatement
| varDeclarationLine ';'                                                                              #varDeclarationStatement
| methodCall ';'                                                                                      #methodCallStatement
| 'return' expression ';'                                                                             #returnStatement
| 'return' ';'                                                                                        #returnVoidStatement
;

varDeclarationLine: CONST? TYPE varDeclaration (',' varDeclaration)*;
varDeclaration:
IDENT ('=' expression)?
| IDENT ('=' IDENT)* '=' expression;

varAssignment: IDENT '=' expression;

expression
: expression op=(AND | OR) expression1
| expression1
;

expression1
: expression1 op=(EQ | NOT_EQ | G | GE | L | LE) expression2
| expression2
;

expression2
: expression2 op=(PLUS | MINUS) expression3
| expression3
;

expression3
: expression3 op=(MUL | DIV | MOD) expression4
| expression4
;

expression4
: op=(NEG | PLUS | MINUS) expression5
| expression5
;

expression5
: '(' expression ')'
| expression6
;

expression6
: val           #expVal
| IDENT         #expIdent
| methodCall    #expMethod
;


methodCall: IDENT '(' ( expression)? (',' expression)* ')';
method: TYPE IDENT '(' (TYPE IDENT)?  (',' TYPE IDENT)* ')' statement;

val
: INT
| decimal
| BOOLEAN
;


AND: '&&';
OR: '||';

G: '>' ;
GE: '>=' ;
L: '<' ;
LE: '<=' ;
EQ: '==';
NOT_EQ: '!=' ;
NEG: '!';

PLUS : '+';
MINUS : '-';
MUL : '*';
DIV : '/';
MOD : '%';

CONST: 'const';
TYPE : 'int' |  'boolean' | 'ratio' | 'void';


INT : [0-9]+;
BOOLEAN: 'true' | 'false';
decimal: INT '.' INT;

IDENT : ([a-z] | [A-Z] | '_')  ([a-z] | [A-Z] | '_' | [0-9])*;

WHITESPACE : [ \r\t\n]+ -> skip ;