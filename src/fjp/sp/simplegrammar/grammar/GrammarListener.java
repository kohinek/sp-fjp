// Generated from C:/Users/stepa/IdeaProjects/sp-fjp/src/fjp/sp/simplegrammar/grammar\Grammar.g4 by ANTLR 4.9
package fjp.sp.simplegrammar.grammar;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link GrammarParser}.
 */
public interface GrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link GrammarParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(GrammarParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(GrammarParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#body}.
	 * @param ctx the parse tree
	 */
	void enterBody(GrammarParser.BodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#body}.
	 * @param ctx the parse tree
	 */
	void exitBody(GrammarParser.BodyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code blockStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterBlockStatement(GrammarParser.BlockStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code blockStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitBlockStatement(GrammarParser.BlockStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement(GrammarParser.IfStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement(GrammarParser.IfStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code forStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterForStatement(GrammarParser.ForStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code forStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitForStatement(GrammarParser.ForStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code whileStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterWhileStatement(GrammarParser.WhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code whileStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitWhileStatement(GrammarParser.WhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code varAssignmentStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterVarAssignmentStatement(GrammarParser.VarAssignmentStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code varAssignmentStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitVarAssignmentStatement(GrammarParser.VarAssignmentStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code varDeclarationStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterVarDeclarationStatement(GrammarParser.VarDeclarationStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code varDeclarationStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitVarDeclarationStatement(GrammarParser.VarDeclarationStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code methodCallStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterMethodCallStatement(GrammarParser.MethodCallStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code methodCallStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitMethodCallStatement(GrammarParser.MethodCallStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code returnStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterReturnStatement(GrammarParser.ReturnStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code returnStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitReturnStatement(GrammarParser.ReturnStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code returnVoidStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterReturnVoidStatement(GrammarParser.ReturnVoidStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code returnVoidStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitReturnVoidStatement(GrammarParser.ReturnVoidStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#varDeclarationLine}.
	 * @param ctx the parse tree
	 */
	void enterVarDeclarationLine(GrammarParser.VarDeclarationLineContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#varDeclarationLine}.
	 * @param ctx the parse tree
	 */
	void exitVarDeclarationLine(GrammarParser.VarDeclarationLineContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#varDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterVarDeclaration(GrammarParser.VarDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#varDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitVarDeclaration(GrammarParser.VarDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#varAssignment}.
	 * @param ctx the parse tree
	 */
	void enterVarAssignment(GrammarParser.VarAssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#varAssignment}.
	 * @param ctx the parse tree
	 */
	void exitVarAssignment(GrammarParser.VarAssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(GrammarParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(GrammarParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#expression1}.
	 * @param ctx the parse tree
	 */
	void enterExpression1(GrammarParser.Expression1Context ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#expression1}.
	 * @param ctx the parse tree
	 */
	void exitExpression1(GrammarParser.Expression1Context ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#expression2}.
	 * @param ctx the parse tree
	 */
	void enterExpression2(GrammarParser.Expression2Context ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#expression2}.
	 * @param ctx the parse tree
	 */
	void exitExpression2(GrammarParser.Expression2Context ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#expression3}.
	 * @param ctx the parse tree
	 */
	void enterExpression3(GrammarParser.Expression3Context ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#expression3}.
	 * @param ctx the parse tree
	 */
	void exitExpression3(GrammarParser.Expression3Context ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#expression4}.
	 * @param ctx the parse tree
	 */
	void enterExpression4(GrammarParser.Expression4Context ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#expression4}.
	 * @param ctx the parse tree
	 */
	void exitExpression4(GrammarParser.Expression4Context ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#expression5}.
	 * @param ctx the parse tree
	 */
	void enterExpression5(GrammarParser.Expression5Context ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#expression5}.
	 * @param ctx the parse tree
	 */
	void exitExpression5(GrammarParser.Expression5Context ctx);
	/**
	 * Enter a parse tree produced by the {@code expVal}
	 * labeled alternative in {@link GrammarParser#expression6}.
	 * @param ctx the parse tree
	 */
	void enterExpVal(GrammarParser.ExpValContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expVal}
	 * labeled alternative in {@link GrammarParser#expression6}.
	 * @param ctx the parse tree
	 */
	void exitExpVal(GrammarParser.ExpValContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expIdent}
	 * labeled alternative in {@link GrammarParser#expression6}.
	 * @param ctx the parse tree
	 */
	void enterExpIdent(GrammarParser.ExpIdentContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expIdent}
	 * labeled alternative in {@link GrammarParser#expression6}.
	 * @param ctx the parse tree
	 */
	void exitExpIdent(GrammarParser.ExpIdentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expMethod}
	 * labeled alternative in {@link GrammarParser#expression6}.
	 * @param ctx the parse tree
	 */
	void enterExpMethod(GrammarParser.ExpMethodContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expMethod}
	 * labeled alternative in {@link GrammarParser#expression6}.
	 * @param ctx the parse tree
	 */
	void exitExpMethod(GrammarParser.ExpMethodContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#methodCall}.
	 * @param ctx the parse tree
	 */
	void enterMethodCall(GrammarParser.MethodCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#methodCall}.
	 * @param ctx the parse tree
	 */
	void exitMethodCall(GrammarParser.MethodCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#method}.
	 * @param ctx the parse tree
	 */
	void enterMethod(GrammarParser.MethodContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#method}.
	 * @param ctx the parse tree
	 */
	void exitMethod(GrammarParser.MethodContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#val}.
	 * @param ctx the parse tree
	 */
	void enterVal(GrammarParser.ValContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#val}.
	 * @param ctx the parse tree
	 */
	void exitVal(GrammarParser.ValContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#decimal}.
	 * @param ctx the parse tree
	 */
	void enterDecimal(GrammarParser.DecimalContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#decimal}.
	 * @param ctx the parse tree
	 */
	void exitDecimal(GrammarParser.DecimalContext ctx);
}