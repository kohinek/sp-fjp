// Generated from C:/Users/stepa/IdeaProjects/sp-fjp/src/fjp/sp/simplegrammar/grammar\Grammar.g4 by ANTLR 4.9
package fjp.sp.simplegrammar.grammar;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link GrammarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface GrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link GrammarParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(GrammarParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBody(GrammarParser.BodyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code blockStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockStatement(GrammarParser.BlockStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStatement(GrammarParser.IfStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code forStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForStatement(GrammarParser.ForStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code whileStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileStatement(GrammarParser.WhileStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code varAssignmentStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarAssignmentStatement(GrammarParser.VarAssignmentStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code varDeclarationStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarDeclarationStatement(GrammarParser.VarDeclarationStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code methodCallStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodCallStatement(GrammarParser.MethodCallStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code returnStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnStatement(GrammarParser.ReturnStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code returnVoidStatement}
	 * labeled alternative in {@link GrammarParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnVoidStatement(GrammarParser.ReturnVoidStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#varDeclarationLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarDeclarationLine(GrammarParser.VarDeclarationLineContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#varDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarDeclaration(GrammarParser.VarDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#varAssignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarAssignment(GrammarParser.VarAssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(GrammarParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#expression1}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression1(GrammarParser.Expression1Context ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#expression2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression2(GrammarParser.Expression2Context ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#expression3}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression3(GrammarParser.Expression3Context ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#expression4}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression4(GrammarParser.Expression4Context ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#expression5}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression5(GrammarParser.Expression5Context ctx);
	/**
	 * Visit a parse tree produced by the {@code expVal}
	 * labeled alternative in {@link GrammarParser#expression6}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpVal(GrammarParser.ExpValContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expIdent}
	 * labeled alternative in {@link GrammarParser#expression6}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpIdent(GrammarParser.ExpIdentContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expMethod}
	 * labeled alternative in {@link GrammarParser#expression6}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpMethod(GrammarParser.ExpMethodContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#methodCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodCall(GrammarParser.MethodCallContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#method}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethod(GrammarParser.MethodContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#val}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVal(GrammarParser.ValContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#decimal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecimal(GrammarParser.DecimalContext ctx);
}