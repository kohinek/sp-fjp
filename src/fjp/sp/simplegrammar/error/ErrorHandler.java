package fjp.sp.simplegrammar.error;

public class ErrorHandler {

    //hlaseni erroru

    private static final ErrorHandler instance = new ErrorHandler();

    private ErrorHandler(){}

    public static ErrorHandler getInstance()
    {
        return instance;
    }

    public void error(String message) {
        System.out.println("Error: " + message);
        System.exit(1);
    }
}
